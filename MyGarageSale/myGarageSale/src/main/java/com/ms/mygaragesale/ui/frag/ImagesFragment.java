package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.toolbox.ImageLoader;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.ui.view.TouchImageView;

public class ImagesFragment extends Fragment implements ViewPager.OnPageChangeListener {
	
	public static final String KEY_IMAGE_URLS = "imageUrls";
	public static final String KEY_CURRENT_IMAGE_INDEX = "currentImageIndex";
	
	private ViewPager viewPagerImages;
	private ArrayList<String> imageUrls;
	private int currentImageIndex;
	private ImagesViewAdapter imagesViewAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		Bundle bundle = getArguments();
		imageUrls = bundle.getStringArrayList(KEY_IMAGE_URLS);
		currentImageIndex = bundle.getInt(KEY_CURRENT_IMAGE_INDEX);
		imagesViewAdapter = new ImagesViewAdapter();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_images, null);
		viewPagerImages = (ViewPager) view.findViewById(R.id.view_pager_images);
		viewPagerImages.setOnPageChangeListener(this);
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.update();
	}
	
	private void update() {
		viewPagerImages.setAdapter(imagesViewAdapter);
		viewPagerImages.setCurrentItem(currentImageIndex, true);
		this.setTitle();
	}

	private void setTitle() {
		((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(
				String.format(getResources().getQuantityString(R.plurals.images_count_plural, imageUrls.size(), imageUrls.size()) + " (%d of %d)", (currentImageIndex + 1), imageUrls.size()));
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	}

	@Override
	public void onPageSelected(int position) {
		this.currentImageIndex = position;
		this.setTitle();
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}

	private class ImagesViewAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imageUrls.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			return view == obj;
		}
		
		@Override
        public View instantiateItem(ViewGroup container, int position) {
            TouchImageView img = new TouchImageView(container.getContext());
            ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
			imageLoader.get(imageUrls.get(position), ImageLoader.getImageListener(img, 0, 0));
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
		
	}

}
