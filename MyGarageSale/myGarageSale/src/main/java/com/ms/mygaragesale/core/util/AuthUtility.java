package com.ms.mygaragesale.core.util;

import android.content.Context;
import android.os.Bundle;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphLocation;
import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.google.gson.internal.LinkedTreeMap;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.Location;
import com.ms.mygaragesale.model.Login;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.google.GLocation;
import com.ms.mygaragesale.model.google.GPlaceDetail;
import com.ms.mygaragesale.model.google.GPlaceDetailResult;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.AuthToken;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.http.GET;
import retrofit.http.Query;

public class AuthUtility {

	private AuthManagerStatusListener authManagerStatusListener;
	private boolean isCancelled;
	
	private AuthUtility() {
	}
	
	public static AuthUtility newInstance(AuthManagerStatusListener listener) {
		AuthUtility authManager = new AuthUtility();
		authManager.authManagerStatusListener = listener;
		return authManager;
	}
	
	public void cancel() {
		this.isCancelled = true;
		this.authManagerStatusListener = null;
	}
	
	private void signupUsingFacebook(AuthStatus authState) {
		if (authState != AuthStatus.SIGNUP_USING_FB_REQUIRE && authState != AuthStatus.READ_USER_DATA_FROM_FB_REQUIRE) {
			throw new IllegalArgumentException("Invalid state to login using facebook");
		}
		if (authState == AuthStatus.READ_USER_DATA_FROM_FB_REQUIRE) {
			if (authManagerStatusListener != null) {
				authManagerStatusListener.onMessage("Reading user data from facebook account..");
			}

			// read data from fb
			final com.facebook.Session session = com.facebook.Session.getActiveSession();
			if (session == null) {
				authFailed(new MGSError(MGSErrorType.FB_LOGIN_ERROR));
				return;
			}
			
			Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
				
				@Override
				public void onCompleted(GraphUser graphUser, com.facebook.Response response) {
					if (isCancelled) {
						return;
					}
					if (graphUser != null) {
						 CurrentUser user = CurrentUser.getCurrentUser();
						 String username = null;
						 if (StringUtil.isNullOrEmpty(graphUser.getUsername())) {
							 if (StringUtil.isNullOrEmpty(graphUser.getProperty("email").toString())) {
								 username = graphUser.getId();
							 } else {
								 username = graphUser.getProperty("email").toString();
							 }
						 } else {
							 username = graphUser.getUsername();
						 }
						 user.setName(graphUser.getName());
						 user.setUsername(username);
						 user.setPassword(username);
						 Object emailIdObj = graphUser.getProperty("email");
						 if (emailIdObj != null && !StringUtil.isNullOrEmpty(emailIdObj.toString())) {
							 user.setEmailId(graphUser.getProperty("email").toString());
						 }
						 
						 GraphPlace graphPlace = graphUser.getLocation();
						 if (graphPlace != null) {
							 user.setAddress(graphPlace.getName());
							 user.setLocation(new Location(graphPlace.getName()));
							 GraphLocation graphLocation = graphPlace.getLocation();
							 if (graphLocation != null) {
								user.getLocation().setLatitude(graphLocation.getLatitude());
								user.getLocation().setLongitude(graphLocation.getLongitude());
								processDeviceCurrentLocation();
							 } else {
								 getFbLocation(session, graphPlace.getId());
							 }
						 } else {
							 processDeviceCurrentLocation();
						 }
						 user.updateCurrentUser();
					} else {
						authFailed(new MGSError(MGSErrorType.FB_LOGIN_ERROR));
					}
				}
			});
			request.executeAsync();
			
			
		} else if (authState == AuthStatus.SIGNUP_USING_FB_REQUIRE) {
			processDeviceCurrentLocation();
		}
	}
	
	private void getFbLocation(com.facebook.Session session, String locationId) {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onMessage("Fetching location..");
		}
		
		Bundle params = new Bundle();
		params.putString("fields", "location");
		Request request = new Request(session, locationId, params, HttpMethod.GET, new Request.Callback() {
			
			@Override
			public void onCompleted(Response response) {
				if (isCancelled) {
					return;
				}
				String respStr = null;
				if (response != null && (respStr = response.getRawResponse()) != null) {
					try {
						JSONObject jsonObject = new JSONObject(respStr);
						JSONObject locationObj = (JSONObject) jsonObject.get("location");
						CurrentUser currentUser = CurrentUser.getCurrentUser();
						currentUser.getLocation().setLatitude(locationObj.getDouble("latitude"));
						currentUser.getLocation().setLongitude(locationObj.getDouble("longitude"));
						currentUser.updateCurrentUser();
						processDeviceCurrentLocation();
					} catch (Exception e) {
						if (Logger.DEBUG) {
							e.printStackTrace();
						}
					}
				} else {
					processDeviceCurrentLocation();
				}
			}
		});
		request.executeAsync();
	}
	
	private void signupUsingTwitter(AuthStatus authState) {
		if (authState == AuthStatus.READ_USER_DATA_FROM_TWITTER_REQUIRE) {
			if (authManagerStatusListener != null) {
				authManagerStatusListener.onMessage("Reading user data from twitter account..");
			}
			
			TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
			if (twitterSession == null) {
				authFailed(new MGSError(MGSErrorType.TWITTER_LOGIN_ERROR));
			}
			// read data from twitter
			TwitterGetUserInfoApiClient twitterApiClient = new TwitterGetUserInfoApiClient(new com.twitter.sdk.android.core.Session(twitterSession.getAuthToken(), twitterSession.getId()));
			TwitterGetUserInfoService twitterGetUserInfoService = twitterApiClient.getTwitterGetUserInfoService();
			twitterGetUserInfoService.show(twitterSession.getUserId(), new Callback<LinkedTreeMap<String, Object>>() {

				@Override
				public void failure(TwitterException arg0) {
					if (isCancelled) {
						return;
					}
					authFailed(new MGSError(MGSErrorType.TWITTER_LOGIN_ERROR));
				}

				@Override
				public void success(Result<LinkedTreeMap<String, Object>> result) {
					if (isCancelled) {
						return;
					}
					LinkedTreeMap<String, Object> map = result.data;
					CurrentUser user = CurrentUser.getCurrentUser();
					// fetch data and populate user
					Object objectScreenName = map.get("screen_name");
					if (objectScreenName != null) {
						user.setUsername(objectScreenName.toString());
						user.setPassword(objectScreenName.toString());
					}
					Object objectName = map.get("name");
					if (objectName != null && StringUtil.isNullOrEmpty(objectName.toString())) {
						user.setName(objectName.toString());
					} else {
						user.setName(objectScreenName.toString());
					}
					Object objectLocation = map.get("location");
					if (objectLocation != null) {
						user.setLocation(new Location(objectLocation.toString()));
					}
					// TODO: get profile image also
					user.updateCurrentUser();
					processDeviceCurrentLocation();
				}
			});
			
		} else if (authState == AuthStatus.SIGNUP_USING_TWITTER_REQUIRE) {
			processDeviceCurrentLocation();
		}
	}
	
	private void getUserLocation() {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onMessage("Fetching location..");
		}
		
		CurrentUser currentUser = CurrentUser.getCurrentUser();
		Location location = currentUser.getLocation();
		String url = null;
		if (location instanceof GLocation) {
			String placeId = ((GLocation) location).getPlaceId();
			url = UrlUtil.getLocationDetailUrl(placeId);
		}
		
		ApiTask<GPlaceDetail> apiTask = new ApiTaskFactory<GPlaceDetail>()
			.newInstance(MGSApplication.getInstance().getApplicationContext(), url, 
					APIMethod.GET, GPlaceDetail.class, new ApiTask.IApiTaskResponseHandler<GPlaceDetail>() {
			
			@Override
			public void onApiSuccess(GPlaceDetail placeDetail, boolean isCachedResponse) {
				if (isCancelled) {
					return;
				}
				Logger.log("get location detail from server success");
				if (placeDetail != null) {
					CurrentUser user = CurrentUser.getCurrentUser();
					Location location = user.getLocation();
					GPlaceDetailResult result = placeDetail.getPlaceDetailResult();
					if (result != null) {
						location.setLatitude(result.getGeometry().getLocation().getLat());
						location.setLongitude(result.getGeometry().getLocation().getLng());
					} else {
						user.setLocation(null);
					}
					user.updateCurrentUser();
				}
				registerUser();
			}
			
			@Override
			public void onApiFailure(MGSError error) {
				if (isCancelled) {
					return;
				}
				Logger.log("get location detail from server error");
				registerUser();
			}
		});
		apiTask.executeRequest(CurrentUser.getCurrentUser(), true);
	}
	
	/**
	 * Method to get device current location if it does not contain any location
	 */
	private void processDeviceCurrentLocation() {
		CurrentUser currentUser = CurrentUser.getCurrentUser();
		if (currentUser.getLocation() != null && currentUser.getLocation().getLatitude() > 0 && currentUser.getLocation().getLongitude() > 0) {
			processSignup();
		} else {
			if (authManagerStatusListener != null) {
				authManagerStatusListener.onFailure(new MGSError(MGSErrorType.LOCATION_UNAVAILABLE_ERROR));
			}
		}
	}

	/**
	 * Method first get location if does not found any else register user
	 */
	public void processSignup() {
		Location location = CurrentUser.getCurrentUser().getLocation();
		if (location != null && location.getLatitude() == 0 && !StringUtil.isNullOrEmpty(location.getName())) {
			getUserLocation();
			return;
		}

		this.registerUser();
	}
	
	/**
	 * Method to add user 
	 */
	private void registerUser() {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onMessage("Registering..");
		}
		
		ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>()
				.newInstance(MGSApplication.getInstance().getApplicationContext(), UrlUtil.getAddUserUrl(), 
						APIMethod.POST, CurrentUser.class, new ApiTask.IApiTaskResponseHandler<CurrentUser>() {
				
				@Override
				public void onApiSuccess(CurrentUser userResponse, boolean isCachedResponse) {
					if (isCancelled) {
						return;
					}
					Logger.log("sign up success");
					if (userResponse != null) {
						CurrentUser user = CurrentUser.getCurrentUser();
						user.copyNotNullValueWithUser(userResponse);
						user.updateCurrentUser();
						processLogin();
					}
				}
				
				@Override
				public void onApiFailure(MGSError error) {
					if (isCancelled) {
						return;
					}
					Logger.log("sign up error");
					authFailed(error);
				}
			});
			apiTask.executeRequest(CurrentUser.getCurrentUser(), false);
	}
	
	public void processLogin() {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onMessage("Logging in..");
		}

		ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>()
				.newInstance(MGSApplication.getInstance().getApplicationContext(), UrlUtil.getLoginUrl(), 
						APIMethod.POST, CurrentUser.class, new ApiTask.IApiTaskResponseHandler<CurrentUser>() {
			@Override
			public void onApiSuccess(CurrentUser currentUser, boolean isCachedResponse) {
				if (isCancelled) {
					return;
				}
				// TODO check for wrong token value
				CurrentUser user = CurrentUser.getCurrentUser();
				user.copyNotNullValueWithUser(currentUser);
				user.updateCurrentUser();
				authSuccess();
			}
			
			@Override
			public void onApiFailure(MGSError error) {
				if (isCancelled) {
					return;
				}
				authFailed(error);
			}
		});
		CurrentUser user = CurrentUser.getCurrentUser();
		Login login = new Login(user.getUsername(), user.getPassword());
		apiTask.executeRequest(login, false);
	}

	/** This method check for if user logged in already or require login or require sign up. Used from splash screen. */
	public void processIncompleteAuthentication() {
		AuthStatus authStatus = getAuthStatus();
		if (authStatus == AuthStatus.LOGGED_IN) {
			if (this.authManagerStatusListener != null) {
				this.authManagerStatusListener.onSuccess();
			}
		} else if (authStatus == AuthStatus.LOG_IN_REQUIRE) {
			processLogin();
		} else {
			if (this.authManagerStatusListener != null) {
				this.authManagerStatusListener.onFailure(new MGSError(MGSErrorType.NO_USER_LOGGED_IN));
			}
		}
	}
	
	public void processAuthentication() {
		AuthStatus authStatus = getAuthStatus();
		if (authStatus == AuthStatus.LOGGED_IN) {
			if (this.authManagerStatusListener != null) {
				this.authManagerStatusListener.onSuccess();
			}
			return;
		}
		if (authStatus == AuthStatus.SIGNUP_REQUIRE) {
			if (this.authManagerStatusListener != null) {
				this.authManagerStatusListener.onFailure(new MGSError(MGSErrorType.NO_USER_LOGGED_IN));
			}
			return;
		}
		if (authStatus == AuthStatus.LOG_IN_REQUIRE) {
			processLogin();
		} else if (authStatus == AuthStatus.SIGNUP_USING_FB_REQUIRE || authStatus == AuthStatus.READ_USER_DATA_FROM_FB_REQUIRE) {
			signupUsingFacebook(authStatus);
		} else if (authStatus == AuthStatus.SIGNUP_USING_TWITTER_REQUIRE || authStatus == AuthStatus.READ_USER_DATA_FROM_TWITTER_REQUIRE) {
			signupUsingTwitter(authStatus);
		} else if (authStatus == AuthStatus.NEW_SIGNUP) {
			processDeviceCurrentLocation();
		}
	}
	
	public AuthStatus getAuthStatus() {
		CurrentUser user = CurrentUser.getCurrentUser();
		if (user.getAccessToken() != null && user.getAccessToken().length() > 0) {
			return AuthStatus.LOGGED_IN;
		} else if (!StringUtil.isNullOrEmpty(user.getUserId())) {
			return AuthStatus.LOG_IN_REQUIRE;
		}
		else if (!StringUtil.isNullOrEmpty(user.getUsername())) {
			if (user.getAccountType() != null && user.getAccountType() == User.ACCOUNT_TYPE_FACEBOOK) {
				return AuthStatus.SIGNUP_USING_FB_REQUIRE;
			} else if (user.getAccountType() != null && user.getAccountType() == User.ACCOUNT_TYPE_TWITTER) {
				return AuthStatus.SIGNUP_USING_TWITTER_REQUIRE;
			} else {
				return AuthStatus.SIGNUP_REQUIRE;
			}
		} else if (user.getAccountType() != null && user.getAccountType() == User.ACCOUNT_TYPE_FACEBOOK) {
			return AuthStatus.READ_USER_DATA_FROM_FB_REQUIRE;
		} else if (user.getAccountType() != null &&  user.getAccountType() == User.ACCOUNT_TYPE_TWITTER) {
			return AuthStatus.READ_USER_DATA_FROM_TWITTER_REQUIRE;
		}
		return AuthStatus.SIGNUP_REQUIRE;
	}
	
	public void processLogout() {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onMessage("Logging out..");
		}
	}
	
	private void authFailed(MGSError error) {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onFailure(error);
		}
	}
	
	private void authSuccess() {
		if (authManagerStatusListener != null) {
			authManagerStatusListener.onSuccess();
		}
	}
	
	
	public interface AuthManagerStatusListener {
		void onSuccess();
		void onFailure(MGSError error);
		void onMessage(String message);
	}
	
	public enum AuthStatus {
		LOGGED_IN,
		LOG_IN_REQUIRE,
		SIGNUP_REQUIRE,
		READ_USER_DATA_FROM_FB_REQUIRE,
		SIGNUP_USING_FB_REQUIRE,
		READ_USER_DATA_FROM_TWITTER_REQUIRE,
		SIGNUP_USING_TWITTER_REQUIRE,
		NEW_SIGNUP
	}
	
	private class TwitterGetUserInfoApiClient extends TwitterApiClient {

		public TwitterGetUserInfoApiClient(Session<AuthToken> session) {
			super(session);
		}
		
		/**
	     * Provide TwitterGetUserInfoService with defined endpoints
	     */
	    public TwitterGetUserInfoService getTwitterGetUserInfoService() {
	        return getService(TwitterGetUserInfoService.class);
	    }
	}
	 
	interface TwitterGetUserInfoService {
	    @GET("/1.1/users/show.json")
	    void show(@Query("id") long id, Callback callback);
	}
	
}
