package com.ms.mygaragesale.ui.view;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntDef;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ms.garagesaledeal.R;

public class FloatingActionButton extends ImageButton {

	public static final int SIZE_NORMAL = 0;
	public static final int SIZE_MINI = 1;

	@Retention(RetentionPolicy.SOURCE)
	@IntDef({ FloatingActionButton.SIZE_NORMAL, FloatingActionButton.SIZE_MINI })
	public @interface FAB_SIZE {
	}

	private static final int HALF_TRANSPARENT_WHITE = Color.argb(128, 255, 255, 255);
	private static final int HALF_TRANSPARENT_BLACK = Color.argb(128, 0, 0, 0);

	int mColorNormal;
	int mColorPressed;
	String mTitle;
	@DrawableRes
	private int mIcon;
	private int mSize;

	private float mCircleSize;
	private float mShadowRadius;
	private float mShadowOffset;
	private int mDrawableSize;

	public FloatingActionButton(Context context) {
		this(context, null);
	}

	public FloatingActionButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs);
	}

	public FloatingActionButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.init(context, attrs);
	}

	void init(Context context, AttributeSet attributeSet) {
		final TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.FloatingActionButton, 0, 0);
		this.mColorNormal = attr.getColor(R.styleable.FloatingActionButton_fab_colorNormal, this.getColor(android.R.color.holo_blue_dark));
		this.mColorPressed = attr.getColor(R.styleable.FloatingActionButton_fab_colorPressed, this.getColor(android.R.color.holo_blue_light));
		this.mSize = attr.getInt(R.styleable.FloatingActionButton_fab_size, FloatingActionButton.SIZE_NORMAL);
		this.mIcon = attr.getResourceId(R.styleable.FloatingActionButton_fab_icon, 0);
		this.mTitle = attr.getString(R.styleable.FloatingActionButton_fab_title);
		attr.recycle();

		this.updateCircleSize();
		this.mShadowRadius = this.getDimension(R.dimen.fab_shadow_radius);
		this.mShadowOffset = this.getDimension(R.dimen.fab_shadow_offset);
		this.updateDrawableSize();

		this.updateBackground();
	}

	private void updateDrawableSize() {
		this.mDrawableSize = (int) (this.mCircleSize + 2 * this.mShadowRadius);
	}

	private void updateCircleSize() {
		this.mCircleSize = this.getDimension(this.mSize == FloatingActionButton.SIZE_NORMAL ? R.dimen.fab_size_normal : R.dimen.fab_size_mini);
	}

	public void setSize(@FAB_SIZE int size) {
		if (size != FloatingActionButton.SIZE_MINI && size != FloatingActionButton.SIZE_NORMAL) {
			throw new IllegalArgumentException("Use @FAB_SIZE constants only!");
		}

		if (this.mSize != size) {
			this.mSize = size;
			this.updateCircleSize();
			this.updateDrawableSize();
			this.updateBackground();
		}
	}

	@FAB_SIZE
	public int getSize() {
		return this.mSize;
	}

	public void setIcon(@DrawableRes int icon) {
		if (this.mIcon != icon) {
			this.mIcon = icon;
			this.updateBackground();
		}
	}

	/**
	 * @return the current Color for normal state.
	 */
	public int getColorNormal() {
		return this.mColorNormal;
	}

	public void setColorNormalResId(@ColorRes int colorNormal) {
		this.setColorNormal(this.getColor(colorNormal));
	}

	public void setColorNormal(int color) {
		if (this.mColorNormal != color) {
			this.mColorNormal = color;
			this.updateBackground();
		}
	}

	/**
	 * @return the current color for pressed state.
	 */
	public int getColorPressed() {
		return this.mColorPressed;
	}

	public void setColorPressedResId(@ColorRes int colorPressed) {
		this.setColorPressed(this.getColor(colorPressed));
	}

	public void setColorPressed(int color) {
		if (this.mColorPressed != color) {
			this.mColorPressed = color;
			this.updateBackground();
		}
	}

	int getColor(@ColorRes int id) {
		return this.getResources().getColor(id);
	}

	float getDimension(@DimenRes int id) {
		return this.getResources().getDimension(id);
	}

	public void setTitle(String title) {
		this.mTitle = title;
		final TextView label = (TextView) this.getTag(R.id.fab_label);
		if (label != null) {
			label.setText(title);
		}
	}

	public String getTitle() {
		return this.mTitle;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		this.setMeasuredDimension(this.mDrawableSize, this.mDrawableSize);
	}

	void updateBackground() {
		final float circleLeft = this.mShadowRadius;
		final float circleTop = this.mShadowRadius - this.mShadowOffset;

		final RectF circleRect = new RectF(circleLeft, circleTop, circleLeft + this.mCircleSize, circleTop + this.mCircleSize);

		final LayerDrawable layerDrawable = new LayerDrawable(
				new Drawable[] {
						this.getResources().getDrawable(this.mSize == FloatingActionButton.SIZE_NORMAL ? R.drawable.fab_bg_normal : R.drawable.fab_bg_mini),
						this.createFillDrawable(circleRect),
						this.createStrokesDrawable(circleRect),
						this.getIconDrawable()
				});

		final float iconOffset = (this.mCircleSize - this.getDimension(R.dimen.fab_icon_size)) / 2f;

		final int iconInsetHorizontal = (int) (this.mShadowRadius + iconOffset);
		final int iconInsetTop = (int) (circleTop + iconOffset);
		final int iconInsetBottom = (int) (this.mShadowRadius + this.mShadowOffset + iconOffset);

		layerDrawable.setLayerInset(3, iconInsetHorizontal, iconInsetTop, iconInsetHorizontal, iconInsetBottom);

		this.setBackgroundCompat(layerDrawable);
	}

	Drawable getIconDrawable() {
		if (this.mIcon != 0) {
			return this.getResources().getDrawable(this.mIcon);
		} else {
			return new ColorDrawable(Color.TRANSPARENT);
		}
	}

	private StateListDrawable createFillDrawable(RectF circleRect) {
		final StateListDrawable drawable = new StateListDrawable();
		drawable.addState(new int[] { android.R.attr.state_pressed }, this.createCircleDrawable(circleRect, this.mColorPressed));
		drawable.addState(new int[] { }, this.createCircleDrawable(circleRect, this.mColorNormal));
		return drawable;
	}

	private Drawable createCircleDrawable(RectF circleRect, int color) {
		final Bitmap bitmap = Bitmap.createBitmap(this.mDrawableSize, this.mDrawableSize, Config.ARGB_8888);
		final Canvas canvas = new Canvas(bitmap);

		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(color);

		canvas.drawOval(circleRect, paint);

		return new BitmapDrawable(this.getResources(), bitmap);
	}

	private int opacityToAlpha(float opacity) {
		return (int) (255f * opacity);
	}

	private Drawable createStrokesDrawable(RectF circleRect) {
		final Bitmap bitmap = Bitmap.createBitmap(this.mDrawableSize, this.mDrawableSize, Config.ARGB_8888);
		final Canvas canvas = new Canvas(bitmap);

		final float strokeWidth = this.getDimension(R.dimen.fab_stroke_width);
		final float halfStrokeWidth = strokeWidth / 2f;

		final RectF outerStrokeRect = new RectF(
				circleRect.left - halfStrokeWidth,
				circleRect.top - halfStrokeWidth,
				circleRect.right + halfStrokeWidth,
				circleRect.bottom + halfStrokeWidth
				);

		final RectF innerStrokeRect = new RectF(
				circleRect.left + halfStrokeWidth,
				circleRect.top + halfStrokeWidth,
				circleRect.right - halfStrokeWidth,
				circleRect.bottom - halfStrokeWidth
				);

		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setStrokeWidth(strokeWidth);
		paint.setStyle(Style.STROKE);

		// outer
		paint.setColor(Color.BLACK);
		paint.setAlpha(this.opacityToAlpha(0.02f));
		canvas.drawOval(outerStrokeRect, paint);

		// inner bottom
		paint.setShader(new LinearGradient(innerStrokeRect.centerX(), innerStrokeRect.top, innerStrokeRect.centerX(), innerStrokeRect.bottom,
				new int[] { Color.TRANSPARENT, FloatingActionButton.HALF_TRANSPARENT_BLACK, Color.BLACK },
				new float[] { 0f, 0.8f, 1f },
				TileMode.CLAMP
				));
		paint.setAlpha(this.opacityToAlpha(0.04f));
		canvas.drawOval(innerStrokeRect, paint);

		// inner top
		paint.setShader(new LinearGradient(innerStrokeRect.centerX(), innerStrokeRect.top, innerStrokeRect.centerX(), innerStrokeRect.bottom,
				new int[] { Color.WHITE, FloatingActionButton.HALF_TRANSPARENT_WHITE, Color.TRANSPARENT },
				new float[] { 0f, 0.2f, 1f },
				TileMode.CLAMP
				));
		paint.setAlpha(this.opacityToAlpha(0.8f));
		canvas.drawOval(innerStrokeRect, paint);

		return new BitmapDrawable(this.getResources(), bitmap);
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void setBackgroundCompat(Drawable drawable) {
		if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
			this.setBackground(drawable);
		} else {
			this.setBackgroundDrawable(drawable);
		}
	}
}
