package com.ms.mygaragesale.ui.frag;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.DefaultConf;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.LogoutUtility;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.Location;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.UserImage;
import com.ms.mygaragesale.model.google.GLocation;
import com.ms.mygaragesale.model.google.GPlace;
import com.ms.mygaragesale.ui.IActivityEventCallback;
import com.ms.mygaragesale.ui.frag.LoadingMessageDialogFragment.DialogFragmentCompleteListener;
import com.ms.mygaragesale.ui.view.ActionImageLayout;
import com.ms.mygaragesale.ui.view.ActionImageLayout.OnImageActionButtonListener;
import com.ms.mygaragesale.ui.view.LocationPredictionView;
import com.ms.ui.view.MaterialEditText;
import com.soundcloud.android.crop.Crop;

public class MyAccountFragment extends RefreshableFragment 
	implements OnImageActionButtonListener, IActivityEventCallback, DialogFragmentCompleteListener {

	private ActionImageLayout image_view_user_image;
	private MaterialEditText edit_text_user_name;
	private EditText edit_text_user_email;
	private EditText edit_text_user_contact;
	private EditText edit_text_user_address;
//	private EditText edit_text_user_location;
//	private Spinner spinner_user_role_type;
//	private TextView text_view_account_status;
	private LocationPredictionView locationPredictionView;
	//private Spinner spinner_user_account_status;
	
	private ArrayAdapter<String> arrAdapterAccountType;
	//private ArrayAdapter<String> arrAdapterAccountStatus;
	
	private Uri imageUri;
	private String newPassword;
	
	private boolean isEditing;
	private boolean canChangePassword;
	private CurrentUser currentUser;
	
	public static MyAccountFragment newInstance() {
		final MyAccountFragment fragment = new MyAccountFragment();
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentUser = CurrentUser.getCurrentUser();
		String[] accountTypeArr = {"User", "Moderator", "Admin"};
		arrAdapterAccountType = new ArrayAdapter<>(getActivity(), R.layout.view_spinner_text, accountTypeArr);
		
//		String[] accountStatusArr = {"Pending", "Active", "Blocked"};
//		arrAdapterAccountStatus = new ArrayAdapter<>(getActivity(), R.layout.view_spinner_text, accountStatusArr);
		
		// update current user data from api
		getUserDataFromApi();
	}
	
	protected void initialize() {
		this.initView();
		this.update();
		this.updateLinkToDialogFragment(true);
	}
	
	private void showConfirmOldPassword(String oldPass, String newPass, String conPass) {
		if (canChangePassword) {
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Change Password");
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_change_password, null);
		final EditText editTextOldPassword = (EditText) view.findViewById(R.id.edit_text_old_password);
		final EditText editTextNewPassword = (EditText) view.findViewById(R.id.edit_text_new_password);
		final EditText editTextConfirmPassword = (EditText) view.findViewById(R.id.edit_text_confirm_password);
		editTextOldPassword.setText(oldPass);
		editTextNewPassword.setText(newPass);
		editTextConfirmPassword.setText(conPass);
		
		builder.setView(view);
		builder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String oldPass = editTextOldPassword.getText().toString();
				String newPass = editTextNewPassword.getText().toString();
				String conPass = editTextConfirmPassword.getText().toString();
				if (currentUser.getPassword().equals(oldPass)) {
					if (newPass != null && newPass.length() >= 5) {
						if (newPass.equals(conPass)) {
							newPassword = newPass;
							MGSApplication.showToast("Password Changed. Please save to update it.", Toast.LENGTH_LONG); 
						} else {
							MGSApplication.showToast("Conform Password and New Password is not matching.", Toast.LENGTH_SHORT);
							showConfirmOldPassword(oldPass, newPass, conPass);
						}
					} else {
						MGSApplication.showToast("New Password length must be greater than 5.", Toast.LENGTH_SHORT);
						showConfirmOldPassword(oldPass, newPass, conPass);
					}
				} else {
					MGSApplication.showToast("Invalid Old Password", Toast.LENGTH_SHORT);
					showConfirmOldPassword(oldPass, newPass, conPass);
				}
			}
		});
		builder.setNegativeButton("CANCEL", null);
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		this.updateLinkToDialogFragment(false);
	}
	
	private void initView() {
		this.setFloatingActionButtonIconResId(R.drawable.ic_pencil_wht_24dp);
		this.image_view_user_image.setActionImageResource(R.drawable.ic_pencil_theme_color_24dp);
		this.showFloatingActionButton();
		this.swipeRefreshLayout.setEnabled(false);
		this.setViewsEnabled(false);
//		spinner_user_role_type.setAdapter(arrAdapterAccountType);
		this.locationPredictionView.setHint("");
		//spinner_user_account_status.setAdapter(arrAdapterAccountStatus);
	}
	
	private void update() {
		showView(ContentViewType.CONTENT_DATA);
		edit_text_user_name.setText(this.currentUser.getName());
		edit_text_user_email.setText(this.currentUser.getEmailId());
		edit_text_user_contact.setText(this.currentUser.getMobileNumber());
		edit_text_user_address.setText(this.currentUser.getAddress());
		if (this.currentUser.getLocation() != null) {
			locationPredictionView.setPlace(new GPlace(this.currentUser.getLocation().getName()));
		}
//		spinner_user_role_type.setSelection(getSpinnerIndexForUserRole(this.currentUser.getUserRole()));
		
		this.setAccountStatusText();
		//spinner_user_account_status.setSelection(getSpinnerIndexForAccountStatus(this.currentUser.getAccountStatus()));
		
		String imageId = CurrentUser.getCurrentUser().getImageId();
		if (!StringUtil.isNullOrEmpty(imageId)) {
			VolleyRequestSingleton requestSingleton = VolleyRequestSingleton.getInstance(getActivity());
			ImageLoader imageLoader = requestSingleton.getImageLoader();
			imageLoader.get(UrlUtil.getImageUrl(imageId), ImageLoader.getImageListener(image_view_user_image.getImageView(),
			         R.drawable.ic_anonymous_avatar_40dp, R.drawable.ic_anonymous_avatar_40dp));
		} else {
			if (currentUser.getUserImage() != null && currentUser.getUserImage().getImageUri() != null) {
				image_view_user_image.setImageUri(Uri.parse(currentUser.getUserImage().getImageUri()));
			}
		}
	}
	
	
	private void setAccountStatusText() {
		int color = 0;
		String text = null;
		int accountStatus = currentUser.getAccountStatus();
		if (accountStatus == User.ACCOUNT_STATUS_ACTIVE) {
			text =  "Account Activated";
			color = getResources().getColor(R.color.green_material_color);
		} else if (accountStatus == User.ACCOUNT_STATUS_BLOCKED) {
			text =  "Account Blocked";
			color = getResources().getColor(R.color.red_material_color);
		} else if (accountStatus == User.ACCOUNT_STATUS_PENDING) {
			text =  "Account Activation Pending";
			color = getResources().getColor(R.color.yellow_material_color);
		} else if (accountStatus == User.ACCOUNT_STATUS_UPGRADE_PENDING) {
			text =  "Account Upgradation Pending";
			color = getResources().getColor(R.color.orange_material_color);
		} else if (accountStatus == User.ACCOUNT_STATUS_DOWNGRADE_PENDING) {
			text =  "Account Downgradation Pending";
			color = getResources().getColor(R.color.orange_material_color);
		}
//		text_view_account_status.setText(text);
//		text_view_account_status.setTextColor(color);
	}
	
	@Override
	public void onClick(View view) {
		if (view == this.floatingActionButton) {
			startEditing();
		}
	}
	
	private void getUserDataFromApi() {
		// load user data and store to server
		ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>().newInstance(getActivity(), 
				UrlUtil.getUserUrl(String.valueOf(CurrentUser.getCurrentUser().getUserId())), 
					APIMethod.GET, CurrentUser.class, new UserResponseHandler());
		apiTask.executeRequest(null, false);
	}
	
//	private int getSpinnerIndexForAccountStatus(int accountStatus) {
//		if (accountStatus == User.ACCOUNT_STATUS_PENDING) {
//			return 0;
//		}
//		if (accountStatus == User.ACCOUNT_STATUS_ACTIVE) {
//			return 1;
//		}
//		if (accountStatus == User.ACCOUNT_STATUS_BLOCKED) {
//			return 2;
//		}
//		return 0;
//	}
//	
//	private int getAccountStatusFromSpinner() {
//		int index = spinner_user_account_status.getSelectedItemPosition();
//		if (index == 0) {
//			return User.ACCOUNT_STATUS_PENDING;
//		}
//		if (index == 1) {
//			return User.ACCOUNT_STATUS_ACTIVE;
//		}
//		if (index == 2) {
//			return User.ACCOUNT_STATUS_BLOCKED;
//		}
//		return 0;
//	}
	
	private int getSpinnerIndexForUserRole(int userRole) {
		if (userRole == User.USER_ROLE_USER) {
			return 0;
		}
		if (userRole == User.USER_ROLE_MODERATOR) {
			return 1;
		}
		if (userRole == User.USER_ROLE_ADMIN) {
			return 2;
		}
		return 0;
	}
	
//	private int getUserRoleFromSpinner() {
//		int index = spinner_user_role_type.getSelectedItemPosition();
//		if (index == 0) {
//			return User.USER_ROLE_USER;
//		}
//		if (index == 1) {
//			return User.USER_ROLE_MODERATOR;
//		}
//		if (index == 2) {
//			return User.USER_ROLE_ADMIN;
//		}
//		return 0;
//	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final ActionBar actionBar = ((ActionBarActivity) this.getActivity()).getSupportActionBar();
		actionBar.setTitle("My Account");
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.my_account, menu);
		if (!isEditing) {
			menu.removeItem(R.id.action_save_account);
			menu.removeItem(R.id.action_change_password);
		} else {
			menu.removeItem(R.id.action_edit_account);
			menu.removeItem(R.id.action_logout);
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result =  super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.action_edit_account:
			this.startEditing();
			result = true;
			break;
			
		case R.id.action_change_password:
			this.showConfirmOldPassword(null, null, null);
			result = true;
			break;
			
		case R.id.action_save_account:
			startSaving();
			result = true;
			break;
		case R.id.action_logout:
			doLogout();
			result = true;
			break;
		default:
			break;
		}
		return result;
	}
	
	private void doLogout() {
		LogoutUtility.processLogout(this.getActivity());
	}
	
	private void startEditing() {
		isEditing = true;
		getActivity().invalidateOptionsMenu();
		setViewsEnabled(true);
		hideFloatingActionButton();
	}
	
	private void startSaving() {
		showFloatingActionButton();
		isEditing = false;
		getActivity().invalidateOptionsMenu();
		setViewsEnabled(false);
		
		if (isDataChanged()) {
			//Toast.makeText(getActivity(), "Saving Account", Toast.LENGTH_SHORT).show();
			
			currentUser.setPassword(newPassword);
			currentUser.setAddress(edit_text_user_address.getText().toString());
			currentUser.setMobileNumber(edit_text_user_contact.getText().toString());
			currentUser.setEmailId(edit_text_user_email.getText().toString());
			// get old user location if user location is same do nothing
			String newLocationName = locationPredictionView.getPlace().getDescription();
			Location location = currentUser.getLocation();
			if (location != null && location.getName() != null) {
				if (!newLocationName.equalsIgnoreCase(location.getName())) {
					if (!StringUtil.isNullOrEmpty(newLocationName)) {
						currentUser.setLocation(new GLocation(locationPredictionView.getPlace()));
					}
				}
			} else {
				if (!StringUtil.isNullOrEmpty(newLocationName)) {
					currentUser.setLocation(new GLocation(locationPredictionView.getPlace()));
				}
			}
			currentUser.setName(edit_text_user_name.getText().toString());
			//currentUser.setAccountStatus(getAccountStatusFromSpinner());
//			int newUserRole = getUserRoleFromSpinner();
//			updateAccountStatus(newUserRole);
//			currentUser.setUserRole(newUserRole);
//			this.setAccountStatusText();
			currentUser.setDataSyncd(false);
			boolean shouldUploadImage = false;
			if (this.imageUri != null) {
				// update image uri to db also 
				com.ms.mygaragesale.model.UserImage userImage = new UserImage(new AppSharedPref().getLongValue(), null, imageUri.toString());
				currentUser.setUserImage(userImage);
				currentUser.setImageId(null);
				this.imageUri = null;
				shouldUploadImage = true;
			} 
			showEditUserDialog(shouldUploadImage);
			currentUser.updateCurrentUser();
			
		} else {
			showFloatingActionButton();
		}
	}
	
	private void updateAccountStatus(int userNewRole) {
		if (currentUser.getUserRole() != userNewRole) {
			if ((userNewRole == User.USER_ROLE_MODERATOR || userNewRole == User.USER_ROLE_ADMIN)) {
				currentUser.setAccountStatus(User.ACCOUNT_STATUS_UPGRADE_PENDING);
			}
		}
	}
	
	private boolean isDataChanged() {
		CurrentUser user = CurrentUser.getCurrentUser();
		if (imageUri != null) {
			return true;
		}
		if (newPassword != null && !newPassword.equals(user.getPassword())) {
			return true;
		}
		if (!edit_text_user_address.getText().toString().equals(user.getAddress())) {
			return true;
		}
		if (!edit_text_user_contact.getText().toString().equals(user.getMobileNumber())) {
			return true;
		}
		if (!edit_text_user_email.getText().toString().equals(user.getEmailId())) {
			return true;
		}
		
		if (locationPredictionView.getPlace() != null) {
			if (user.getLocation() == null || user.getLocation().getName() == null) {
				return true;
			} 
			if (!locationPredictionView.getPlace().getDescription().equalsIgnoreCase(user.getLocation().getName())) {
				return true;
			} 
		}
		if (!edit_text_user_name.getText().toString().equals(user.getName())) {
			return true;
		}
//		if (spinner_user_account_status.getSelectedItemPosition() != this.getSpinnerIndexForAccountStatus(user.getAccountStatus())) {
//			return true;
//		}
//		if (spinner_user_role_type.getSelectedItemPosition() != this.getSpinnerIndexForUserRole(user.getUserRole())) {
//			return true;
//		}
		return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent result) {
		if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK) {
			this.beginCrop(result.getData());
		} else if (requestCode == Crop.REQUEST_CROP) {
			this.handleCrop(resultCode, result);
		}
	}

	private void beginCrop(Uri source) {
		Uri outputUri = Uri.parse(DefaultConf.getImageCachePath());
		Crop crop = new Crop(source).output(outputUri).asSquare().withMaxSize(DefaultConf.IMAGE_MAX_WIDTH, DefaultConf.IMAGE_MAX_HEIGHT);
		crop.start(getActivity());
		//this.getActivity().startActivityForResult(crop.start(getActivity()), Crop.REQUEST_CROP);
	}

	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == Activity.RESULT_OK) {
			this.imageUri = Crop.getOutput(result);
			Logger.log("captured image uri: " + imageUri.toString());
			this.image_view_user_image.setImageUri(this.imageUri);
		} else if (resultCode == Crop.RESULT_ERROR) {
			MGSApplication.showToast(Crop.getError(result).getMessage(), Toast.LENGTH_SHORT);
		}
	}
	
	private void setViewsEnabled(boolean enable) {
		this.image_view_user_image.setActionButtonEnabled(enable);
		this.edit_text_user_name.setEnabled(enable);
		this.edit_text_user_email.setEnabled(enable);
		this.edit_text_user_contact.setEnabled(enable);
		this.edit_text_user_address.setEnabled(enable);
		this.locationPredictionView.setEnabled(enable);
		
//		if (currentUser.getAccountStatus() == User.ACCOUNT_STATUS_PENDING
//				|| currentUser.getAccountStatus() == User.ACCOUNT_STATUS_BLOCKED) {
//			this.spinner_user_role_type.setEnabled(false);
//		} else {
//			this.spinner_user_role_type.setEnabled(enable);
//		}
//
//		// disable spinner if current user is admin
//		if (currentUser.getUsername().equalsIgnoreCase("admin")) {
//			this.spinner_user_role_type.setEnabled(false);
//			this.spinner_user_role_type.setBackgroundColor(Color.WHITE);
//		}
	}

	@Override
	public void onImageActionClick(ActionImageLayout actionImageLayout) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
		try {
			this.startActivityForResult(intent, Crop.REQUEST_PICK);
		} catch (ActivityNotFoundException e) {
			MGSApplication.showToast(getResources().getString(R.string.crop__pick_error), Toast.LENGTH_SHORT);
		}
	}
		
//	private void uploadUserImage() {
//		// upload user image to server
//		ApiTask<ImagesResponse> apiTask = new ApiTaskFactory<ImagesResponse>().newImageUploadInstance(getActivity(), UrlUtil.getAddImageUrl(), 
//				APIMethod.POST, ImagesResponse.class, new UploadImageResponse());
//		File[] files = { new File(currentUser.getUserImage().getImageUri()) };
//		apiTask.uploadFileRequest(files);
//	}
//	
//	private void updateUserData() {
//		// update user image to server
//		ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>().newInstance(getActivity(), 
//				UrlUtil.getEditUserUrl(String.valueOf(currentUser.getUserId())), 
//				APIMethod.POST, CurrentUser.class, new UpdateUserResponse());
//		apiTask.executeRequest(currentUser, false);
//	}
//	
//	private class UploadImageResponse extends ApiResponseHandler<ImagesResponse> {
//		@Override
//		public void onApiSuccess(ImagesResponse response, boolean isCachedResponse) {
//			IdResponse imageIdResponse = response.getImages()[0]; 
//			File file = new File(currentUser.getUserImage().getImageUri().toString());
//			file.delete();
//			currentUser.getUserImage().setImageUri(null);
//			currentUser.setUserImage(null);
//			currentUser.setImageId(imageIdResponse.getId());
//			Logger.log("image uploaded id: " + imageIdResponse.getId());
//			currentUser.updateCurrentUser();
//			updateUserData();
//		}
//
//		@Override
//		public void onApiFailure(MGSError error) {
//			Logger.log("image upload fail with error " + error.getMesssage());
//			// TODO dialog or toast
//		}
//	}
//	
//	private class UpdateUserResponse extends ApiResponseHandler<CurrentUser> {
//		@Override
//		public void onApiSuccess(CurrentUser response, boolean isCachedResponse) {
//			showFloatingActionButton();
//			Logger.log("user updated successfully");
//			if (response != null) {
//				CurrentUser user = CurrentUser.getCurrentUser();
//				user.copyNotNullValueWithUser(response);
//				user.setDataSyncd(true);
//				user.updateCurrentUser();
//			}
//		}
//
//		@Override
//		public void onApiFailure(MGSError error) {
//			Logger.log("image upload fail with error " + error.getMesssage());
//			// TODO dialog or toast
//		}
//	}

	@Override
	protected View getContentView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.fragment_my_account, null);
		this.image_view_user_image = (ActionImageLayout) view.findViewById(R.id.image_view_user_image);
		this.image_view_user_image.setOnImageActionButtonListener(this);
		this.edit_text_user_name = (MaterialEditText) view.findViewById(R.id.edit_text_user_name);
		this.edit_text_user_email = (EditText) view.findViewById(R.id.edit_text_user_email);
		this.edit_text_user_contact = (EditText) view.findViewById(R.id.edit_text_user_contact);
		this.edit_text_user_address = (EditText) view.findViewById(R.id.edit_text_user_address);
		this.locationPredictionView = (LocationPredictionView) view.findViewById(R.id.edit_text_user_location);
//		this.spinner_user_role_type = (Spinner) view.findViewById(R.id.spinner_user_account_type);
		//this.spinner_user_account_status = (Spinner) view.findViewById(R.id.spinner_user_account_status);
//		this.text_view_account_status = (TextView) view.findViewById(R.id.text_view_account_status);
		return view;
	}

	@Override
	public boolean onActivityBackPressed() {
		if (isEditing && isDataChanged()) {
			this.showDiscardDialog();
		} else {
			this.getActivity().finish();
		}
		return true;
	}
	
	private class UserResponseHandler extends ApiResponseHandler<CurrentUser> {
		@Override
		public void onApiSuccess(CurrentUser response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			if (response != null) {
				CurrentUser user = CurrentUser.getCurrentUser();
				user.copyNotNullValueWithUser(response);
				user.updateCurrentUser();
				update();
			}
		}
	}
	
	private void showDiscardDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Confirmation");
		builder.setMessage("Do you want to discard the changes?");
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.setPositiveButton("DISCARD", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				// delete current image files from cache
				getActivity().finish();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		
	}
	
	private void showEditUserDialog(boolean shouldUploadImage) {
		EditUserDialogFragment editUserDialogFragment = EditUserDialogFragment.newInstance(shouldUploadImage);
		editUserDialogFragment.setDialogFragmentCompleteListener(this);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment prev = fragmentManager.findFragmentByTag("dialog");
	    if (prev != null) {
	    	transaction.remove(prev);
	    }
	    transaction.addToBackStack(null);
	    editUserDialogFragment.show(transaction, "dialog");
	}

	@Override
	public void onComplete(MGSError error) {
		if (getActivity() != null) {
			showFloatingActionButton();
			update();
		}
	}
	
	private void updateLinkToDialogFragment(boolean enable) {
		Fragment prev = getFragmentManager().findFragmentByTag("dialog");
	    if (prev != null && prev instanceof EditUserDialogFragment) {
	    	DialogFragmentCompleteListener listener = this;
	    	if (!enable) {
				listener = null;
			}
	    	((EditUserDialogFragment) prev).setDialogFragmentCompleteListener(listener);
	    }
	}
}
