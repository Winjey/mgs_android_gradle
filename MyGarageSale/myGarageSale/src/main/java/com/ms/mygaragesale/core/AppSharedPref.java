package com.ms.mygaragesale.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.ms.mygaragesale.ui.frag.SaleItemsFragment.SaleItemViewType;

public class AppSharedPref {
	
	private SharedPreferences sharedPreferences;
	private Editor editor;
	
	public AppSharedPref() {
		this.sharedPreferences = MGSApplication.getInstance().getSharedPreferences("APP_SHARED_PREFS", Context.MODE_PRIVATE);
	}
	
	/**
	 * Method used to get long value, used for db objects id
	 * @return
	 */
	public long getLongValue() {
		this.editor = this.sharedPreferences.edit();
		long value = this.sharedPreferences.getLong("LONG_VALUE", 100);
		this.editor.putLong("LONG_VALUE", ++value);
		this.editor.commit();
		return value;
	}
	
	public void setUserLastUpdatedTime(long time) {
		this.editor = this.sharedPreferences.edit();
		this.editor.putLong("USER_LAST_UPDATED_TIME", time);
		this.editor.commit();
	}

	public long getUserLastUpdatedTime() {
		return this.sharedPreferences.getLong("USER_LAST_UPDATED_TIME", 0);
	}
	
//	public void setAllPostLastUpdatedTime(long dateTime) {
//		this.editor = this.sharedPreferences.edit();
//		this.editor.putLong("ALL_POST_LAST_UPDATED_TIME", dateTime);
//		this.editor.commit();
//	}
//
//	public long getAllPostLastUpdatedTime() {
//		return this.sharedPreferences.getLong("ALL_POST_LAST_UPDATED_TIME", 0);
//	}
	
	public void setSaleItemsLastUpdatedTime(SaleItemViewType saleItemViewType, long dateTime) {
		this.editor = this.sharedPreferences.edit();
		this.editor.putLong(saleItemViewType.name() + "_LAST_UPDATED_TIME", dateTime);
		this.editor.commit();
	}

	public long getSaleItemsLastUpdatedTime(SaleItemViewType saleItemViewType) {
		return this.sharedPreferences.getLong(saleItemViewType.name() + "_LAST_UPDATED_TIME", 0);
	}
	
//	public void setMessageListLastUpdatedTime(String dateTime) {
//		this.editor = this.sharedPreferences.edit();
//		this.editor.putString("MESSAGE_LIST_LAST_UPDATED_TIME", dateTime);
//		this.editor.commit();
//	}
//
//	public String getMessageListLastUpdatedTime() {
//		return this.sharedPreferences.getString("MESSAGE_LIST_LAST_UPDATED_TIME", null);
//	}
	
	public void setConversationsLastUpdatedTime(Long timeInMillis) {
		this.editor = this.sharedPreferences.edit();
		this.editor.putLong("CONVERSATIONS_LAST_UPDATED_TIME", timeInMillis);
		this.editor.commit();
	}

	public long getConversationsLastUpdatedTime() {
		return this.sharedPreferences.getLong("CONVERSATIONS_LAST_UPDATED_TIME", 0);
	}
	
	public void setRequestListLastUpdatedTime(long timeInMillis) {
		this.editor = this.sharedPreferences.edit();
		this.editor.putLong("REQUEST_LIST_LAST_UPDATED_TIME", timeInMillis);
		this.editor.commit();
	}

	public long getRequestListLastUpdatedTime() {
		return this.sharedPreferences.getLong("REQUEST_LIST_LAST_UPDATED_TIME", 0);
	}
	
	public void setAlertKeywordsLastUpdatedTime(long timeInMillis) {
		this.editor = this.sharedPreferences.edit();
		this.editor.putLong("ALERT_KEYWORDS_LAST_UPDATED_TIME", timeInMillis);
		this.editor.commit();
	}

	public long getAlertKeywordsLastUpdatedTime() {
		return this.sharedPreferences.getLong("ALERT_KEYWORDS_LAST_UPDATED_TIME", 0);
	}

    public String getGCMRegistrationId() {
        return this.sharedPreferences.getString("registration_id", null);
    }

    public void setGCMRegistrationId(String regId) {
        this.editor = this.sharedPreferences.edit();
        this.editor.putString("registration_id", regId);
        this.editor.commit();
    }

    public int getAppRegisteredVersion() {
        return this.sharedPreferences.getInt("APP_VERSION", 0);
    }

    public void setAppRegisteredVersion(int version) {
        this.editor = this.sharedPreferences.edit();
        this.editor.putInt("APP_VERSION", version);
        this.editor.commit();
    }
	
}
