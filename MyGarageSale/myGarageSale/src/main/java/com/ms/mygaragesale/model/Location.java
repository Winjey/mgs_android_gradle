package com.ms.mygaragesale.model;

import java.io.Serializable;

import com.ms.mygaragesale.core.db.dao.UserLocationTable;

public class Location implements Serializable {

	private static final long serialVersionUID = -19093846132239737L;
	
	// default id is provided to store this location value to db, since there can be only one location id kept same.
	private long id = 1000;
	private String name;
	private double latitude;
	private double longitude;
	
	public Location() {
	}
	
	public Location(String name) {
		super();
		this.name = name;
	}

	public Location(String name, double latitude, double longitude) {
		super();
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Location(UserLocationTable dbUserLocationTable) {
    	if (dbUserLocationTable != null) {
    		this.id = dbUserLocationTable.getId();
            this.name = dbUserLocationTable.getName();
            this.latitude = dbUserLocationTable.getLatitude();
            this.longitude = dbUserLocationTable.getLongitude();
		}
    }
	
	public UserLocationTable getDbUserLocationTable() {
    	UserLocationTable userLocationTable = new UserLocationTable(id, name, latitude, longitude);
    	return userLocationTable;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
		
}
