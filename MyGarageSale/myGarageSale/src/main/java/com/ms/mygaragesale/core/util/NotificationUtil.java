package com.ms.mygaragesale.core.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.ui.MainActivity;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;

public class NotificationUtil {

    public static final String KEY_NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
    public static final String KEY_NOTIFICATION_ID = "NOTIFICATION_ID";
    public static final int NOTIFICATION_TYPE_SALE_ITEM_ALERT = 2000;

    public static final String KEY_NOTIFICATION_BUNDLE = "NOTIFICATION_BUNDLE";
    public static final String KEY_NOTIFICATION_SALE_ITEM_OBJECT = "NOTIFICATION_SALE_ITEM_OBJECT";

    public static final String ITEM_ALERT_TITLE = "New Sales Alert From MGS";

    public static void createNewItemAlertNotification(SaleItem saleItem) {
        ArrayList<AlertKeyword> alertKeywords = DatabaseProvider.getInstance().getAlertKeywords();
        if (alertKeywords == null || alertKeywords.isEmpty()) {
            return;
        }
        HashSet<String> itemAlertKeywords = getItemAlertKeywords(alertKeywords, saleItem);

        // get all alert keywords
        StringBuilder keywords = new StringBuilder();
        for (String alertKeyword : itemAlertKeywords) {
            keywords.append("\"");
            keywords.append(alertKeyword);
            keywords.append("\", ");
        }

        int notificationId = generateSaleItemNotificationId(saleItem.getItemId());
        if (keywords.length() > 0) {
            // remove last ", "
            keywords.replace(keywords.lastIndexOf(", "), keywords.length(), ".");
            String description = "Watch list alert for - "+ keywords;
            Bundle bundle = new Bundle();
            bundle.putSerializable(KEY_NOTIFICATION_SALE_ITEM_OBJECT, saleItem);
            bundle.putInt(KEY_NOTIFICATION_TYPE, NOTIFICATION_TYPE_SALE_ITEM_ALERT);
            bundle.putInt(KEY_NOTIFICATION_ID, notificationId);

            int smallIconResId = R.drawable.ic_shopping_basket_white_24dp;
            int bigIconResId = R.mipmap.ic_launcher;
            sendNotification(ITEM_ALERT_TITLE, description, smallIconResId, bigIconResId, bundle, notificationId);
        }

    }

    private static void sendNotification(String title, String message, int smallIconResId, int bigIconResId, Bundle data, int notificationId) {
        Context appContext = MGSApplication.getInstance().getApplicationContext();
        Bitmap bigIcon = BitmapFactory.decodeResource(appContext.getResources(), bigIconResId);
        // create intent to invoke as pending intent
        Intent intent = new Intent(appContext, MainActivity.class);
        intent.putExtra(KEY_NOTIFICATION_BUNDLE, data);
        PendingIntent pendingIntent = PendingIntent.getActivity(appContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(appContext)
                                                .setSmallIcon(smallIconResId)
                                                .setContentTitle(title)
                                                .setContentText(message)
                                                .setLargeIcon(bigIcon)
                                                .setAutoCancel(true)
                                                .setDefaults(Notification.DEFAULT_ALL)
                                                .setContentIntent(pendingIntent);

        Notification notification = new NotificationCompat.BigTextStyle(builder)
                                        .bigText(message)
                                        .setBigContentTitle(title)
                                        .build();
        NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notification);
    }

    /**
     * Method to remove notification from notification bar and also reset keyword strings array.
     */
    public static void resetItemAlertsNotification(int notificationId) {
        Context appContext = MGSApplication.getInstance().getApplicationContext();
        NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);

    }

    private static HashSet<String> getItemAlertKeywords(ArrayList<AlertKeyword> myAlertKeywords, SaleItem saleItem) {
        HashSet<String> itemAlertKeywords = new HashSet<>();
        for (AlertKeyword alertKeyword : myAlertKeywords) {
            String keywordName = alertKeyword.getKeywordName();
            if (keywordName.equalsIgnoreCase(saleItem.getTitle())
                    || keywordName.equalsIgnoreCase(saleItem.getDescription())
                        || (saleItem.getCategories() != null && saleItem.getCategories().contains(keywordName))) {
                itemAlertKeywords.add(keywordName);
            }
        }
        return itemAlertKeywords;
    }

    // generate notification id based on sale item id, it is also helpful if same item alert comes multiple times, in such cases it updates same one
    private static int generateSaleItemNotificationId(String saleitemId) {
        int notificationId = 1;
        try {
            BigInteger integer = new BigInteger(saleitemId, 36);
            notificationId = Math.abs(integer.intValue());
        } catch (Throwable t) {
            if (!Logger.DEBUG) {
                t.printStackTrace();
            }
        }
        return notificationId;
    }
}
