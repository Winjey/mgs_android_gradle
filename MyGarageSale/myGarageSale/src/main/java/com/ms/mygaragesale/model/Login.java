package com.ms.mygaragesale.model;

import com.ms.mygaragesale.core.AppSharedPref;

public class Login {

    private static final int DEVICE_TYPE_ANDROID = 1;

	private String username;
	private String password;
    private String pushNotificationDeviceId;
    private int deviceType;

	public Login(String username, String password) {
		super();
		this.username = username;
		this.password = password;
        AppSharedPref sharedPref = new AppSharedPref();
        pushNotificationDeviceId = sharedPref.getGCMRegistrationId();
        deviceType = DEVICE_TYPE_ANDROID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public String getPushNotificationDeviceId() {
        return pushNotificationDeviceId;
    }

    public void setPushNotificationDeviceId(String pushNotificationDeviceId) {
        this.pushNotificationDeviceId = pushNotificationDeviceId;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }
}
