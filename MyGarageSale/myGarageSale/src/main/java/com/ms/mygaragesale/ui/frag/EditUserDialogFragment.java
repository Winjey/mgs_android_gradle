package com.ms.mygaragesale.ui.frag;

import java.io.File;

import android.os.Bundle;
import android.widget.Toast;

import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.Location;
import com.ms.mygaragesale.model.google.GLocation;
import com.ms.mygaragesale.model.google.GPlaceDetail;
import com.ms.mygaragesale.model.google.GPlaceDetailResult;
import com.ms.mygaragesale.model.response.IdResponse;
import com.ms.mygaragesale.model.response.ImagesResponse;

public class EditUserDialogFragment extends LoadingMessageDialogFragment {

	private boolean shouldUploadImage;
	private boolean isUploadSuccess;
	

	public static EditUserDialogFragment newInstance(boolean shouldUploadImage) {
		EditUserDialogFragment addNewSaleItemDialogFragment = new EditUserDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putBoolean("KEY_SHOULD_UPLOAD_IMAGE", shouldUploadImage);
		addNewSaleItemDialogFragment.setArguments(bundle);
		return addNewSaleItemDialogFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getArguments();
		this.shouldUploadImage = bundle.getBoolean("KEY_SHOULD_UPLOAD_IMAGE", false);
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		if (!isUploadSuccess && shouldUploadImage) {
			uploadUserImage();
		} else {
			updateLocation();
		}
	}
	
	@Override
	protected void initView() {
		super.initView();
		if (!isProcessStarted) {
			isProcessStarted = true;
			this.editUser();
		}
	}

	private void editUser() {
		if (shouldUploadImage) {
			uploadUserImage();
		} else {
			updateLocation();
		}
	}
	
	private void uploadUserImage() {
		loadingView.setLoadingMesage("Fetching user location..");
		
		// upload user image to server
		ApiTask<ImagesResponse> apiTask = new ApiTaskFactory<ImagesResponse>().newImageUploadInstance(getActivity(), UrlUtil.getAddImageUrl(), 
				APIMethod.POST, ImagesResponse.class, new UploadImageResponse());
		File[] files = { new File(CurrentUser.getCurrentUser().getUserImage().getImageUri()) };
		apiTask.uploadFileRequest(files);
	}
	
	private void updateLocation() {
		
		// check for location
		Location location = CurrentUser.getCurrentUser().getLocation();
		if (location != null && location.getLatitude() == 0 && !StringUtil.isNullOrEmpty(location.getName())) {
			getUserLocation();
			return;
		}
		
		updateUserData();
	}
	
	private void updateUserData() {
		loadingView.setLoadingMesage("Updating user data..");
		
		// update user data to server
		ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>().newInstance(getActivity(), 
				UrlUtil.getEditUserUrl(String.valueOf(CurrentUser.getCurrentUser().getUserId())), 
				APIMethod.POST, CurrentUser.class, new UpdateUserResponse());
		apiTask.executeRequest(CurrentUser.getCurrentUser(), false);
	}
	
	private void getUserLocation() {
		loadingView.setLoadingMesage("Updating user data..");
		
		CurrentUser currentUser = CurrentUser.getCurrentUser();
		Location location = currentUser.getLocation();
		String url = null;
		if (location instanceof GLocation) {
			String placeId = ((GLocation) location).getPlaceId();
			url = UrlUtil.getLocationDetailUrl(placeId);
		}
		
		ApiTask<GPlaceDetail> apiTask = new ApiTaskFactory<GPlaceDetail>()
			.newInstance(MGSApplication.getInstance().getApplicationContext(), url, 
					APIMethod.GET, GPlaceDetail.class, new ApiTask.IApiTaskResponseHandler<GPlaceDetail>() {
			
			@Override
			public void onApiSuccess(GPlaceDetail placeDetail, boolean isCachedResponse) {
				Logger.log("get location detail from server success");
				if (placeDetail != null) {
					CurrentUser user = CurrentUser.getCurrentUser();
					Location location = user.getLocation();
					GPlaceDetailResult result = placeDetail.getPlaceDetailResult();
					if (result != null) {
						location.setLatitude(result.getGeometry().getLocation().getLat());
						location.setLongitude(result.getGeometry().getLocation().getLng());
					} else {
						user.setLocation(null);
					}
					user.updateCurrentUser();
				}
				updateUserData();
			}
			
			@Override
			public void onApiFailure(MGSError error) {
				Logger.log("get location detail from server error");
				updateUserData();
			}
		});
		apiTask.executeRequest(CurrentUser.getCurrentUser(), true);
	}
	
	private class UploadImageResponse extends ApiResponseHandler<ImagesResponse> {
		@Override
		public void onApiSuccess(ImagesResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			IdResponse imageIdResponse = response.getImages()[0]; 
			CurrentUser currentUser = CurrentUser.getCurrentUser();
			File file = new File(currentUser.getUserImage().getImageUri().toString());
			file.delete();
			currentUser.getUserImage().setImageUri(null);
			currentUser.setUserImage(null);
			currentUser.setImageId(imageIdResponse.getId());
			Logger.log("image uploaded id: " + imageIdResponse.getId());
			currentUser.updateCurrentUser();
			updateLocation();
		}

		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			if (!isDialogDismissed) {
				messageView.showMessage(error);
				showView(ContentViewType.MESSAGE);
			} else {
				MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
				EditUserDialogFragment.this.dismiss();
				if (dialogFragmentCompleteListener != null) {
					dialogFragmentCompleteListener.onComplete(error);
				}
			}
		}
	}
	
	private class UpdateUserResponse extends ApiResponseHandler<CurrentUser> {
		@Override
		public void onApiSuccess(CurrentUser response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			Logger.log("user updated successfully");
			if (response != null) {
				CurrentUser user = CurrentUser.getCurrentUser();
				user.copyNotNullValueWithUser(response);
				user.setDataSyncd(true);
				user.updateCurrentUser();
			}
			EditUserDialogFragment.this.dismiss();
			if (dialogFragmentCompleteListener != null) {
				dialogFragmentCompleteListener.onComplete(null);
			}
			MGSApplication.showToast("User account updated.", Toast.LENGTH_LONG);
		}

		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			if (!isDialogDismissed) {
				messageView.showMessage(error);
				showView(ContentViewType.MESSAGE);
			} else {
				MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
				EditUserDialogFragment.this.dismiss();
				if (dialogFragmentCompleteListener != null) {
					dialogFragmentCompleteListener.onComplete(error);
				}
			}
		}
	}

}
