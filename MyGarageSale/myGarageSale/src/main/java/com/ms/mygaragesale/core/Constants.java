package com.ms.mygaragesale.core;

public class Constants {
	
	public static final String CLOSE_ALL_ACTIVITY_LOGOUT_ACTION = "com.ms.mygaragesale.CloseAllActivityLogout";
	public static final String CLOSE_ALL_ACTIVITY_ACCOUNT_BLOCKED_ACTION = "com.ms.mygaragesale.CloseAllActivityAccountBlocked";

	// test fb group
	public static final String FB_GROUP_ID = "1416382651998653";

    public static final String GCM_SENDER_ID = "572343183495";
    public static final String GCM_MESSAGE_NEW_SALE_ITEM_ADDED = "saleItemAdded";

}
