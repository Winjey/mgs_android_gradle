package com.ms.mygaragesale.core.api;

import java.io.File;
import java.net.UnknownHostException;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;
import com.ms.mygaragesale.core.util.MultipartUtility;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.response.Response;

/**
 * Use this class foe file upload operation only.
 */
public class ImageUploadTask<T extends Response> extends ApiTask<T> {

	private ImageUploadAsyncTask imageUploadAsyncTask;

	protected ImageUploadTask(Context context, String url, APIMethod method,
			Class<T> responseObjectClass, IApiTaskResponseHandler<T> apiTaskResponseHandler) {
		super(context, url, method, responseObjectClass, apiTaskResponseHandler);
	}

	@Override
	public void executeRequest(Object requestObject, boolean shouldResultCachedResponse) {
		throw new IllegalArgumentException("Please dont use this method to send/receive json response.");
	}

	@Override
	public void uploadFileRequest(File[] files) {
		imageUploadAsyncTask = new ImageUploadAsyncTask();
		imageUploadAsyncTask.execute(files);
	}

	@Override
	public void cancel() {
		this.apiTaskResponseHandler = null;
		if (imageUploadAsyncTask != null) {
			imageUploadAsyncTask.cancel(true);
			imageUploadAsyncTask = null;
		}
	}

	private class ImageUploadAsyncTask extends AsyncTask<File, Void, T> {

		private MGSError error;

		@Override
		protected T doInBackground(File... params) {
			T result = null;
			try {
				Logger.log("upload image");
				MultipartUtility multipartUtility = new MultipartUtility(url, "UTF-8", CurrentUser.getCurrentUser().getAccessToken());
				for (int i = 0; i < params.length; i++) {
					multipartUtility.addFilePart("uploadImage" + i, params[i]);
				}
				//multipartUtility.addHeaderField("Authorization", User.getCurrentUser().getAccessToken());
				String response = multipartUtility.finish();
				Logger.log("image upload response: " + response);
				result = new Gson().fromJson(response, responseObjectClass);
			} catch (UnknownHostException e) {
				error = new MGSError(MGSErrorType.NO_NETWORK_CONNECTION);
				if (Logger.DEBUG) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
				error = new MGSError(MGSErrorType.API_ERROR);
				if (Logger.DEBUG) {
					e.printStackTrace();
				}
			}
			return result;
		}

		@Override
		protected void onPostExecute(T result) {
			super.onPostExecute(result);
			if (apiTaskResponseHandler != null) {
				if (error != null) {
					apiTaskResponseHandler.onApiFailure(error);
				} else {
					if (result.getErrorCode() > 0) {
						MGSError error = new MGSError(MGSErrorType.API_ERROR, result.getErrorMessage());
						apiTaskResponseHandler.onApiFailure(error);
					} else {
						apiTaskResponseHandler.onApiSuccess(result, false);
					}
				}
			}
		}
	}
}
