package com.ms.mygaragesale.ui.frag;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etsy.android.grid.StaggeredGridView;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.model.response.SaleItemsResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;

public class SearchFragment extends RefreshableItemsWithCountHeaderFragment<SaleItem> implements OnClickListener {
	
	private static final int REQUEST_CODE_SALE_ITEM_DETAIL = 1001;
	
	private ApiTask<SaleItemsResponse> searchResultsApiTask;
	
	private HashSet<Integer> hashSetForOneTimeAnimation;
	private String queryString;

	public static SearchFragment newInstance(String query) {
		final SearchFragment fragment = new SearchFragment();
		Bundle bundle = new Bundle();
		bundle.putString("KEY_QUERY_STRING", query);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRetainInstance(true);
		hashSetForOneTimeAnimation = new HashSet<>();
		Bundle bundle = getArguments();
		queryString = bundle.getString("KEY_QUERY_STRING");
		getSearchResultsFromApi(false);
	}
	
	@Override
	protected long getLastUpdatedTime() {
		return 0;
	}
	
	private void getSearchResultsFromApi(boolean shouldResultCacheResponse) {
		String url = UrlUtil.getSearchKeywordUrl(queryString);
		searchResultsApiTask = new ApiTaskFactory<SaleItemsResponse>().newInstance(getActivity(), 
				url, APIMethod.GET, SaleItemsResponse.class, new GetSaleItemResponseHandler());
		searchResultsApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		initView();
		update();
	}
	
	private void initView() {
		swipeRefreshLayout.setEnabled(false);
		this.showView(ContentViewType.CONTENT_LOADING);
		hideFloatingActionButton();
		this.timestampViewLastUpdated.setVisibility(View.GONE);
		this.headerView.setBackgroundColor(Color.WHITE);
		this.textViewItemsCount.setTextColor(getResources().getColor(R.color.title_text_color));
		this.absListView.setOnItemClickListener(this);
	}
	
	@Override
	protected void setLastUpdatedTimeStamp(long lastUpdatedTime) {
		// override super do nothing
	}
	
	protected void update() {
		if (searchResultsApiTask == null && getActivity() != null) {
			super.update();
		}
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (searchResultsApiTask != null) {
			searchResultsApiTask.cancel();
		}
	}

//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		final View rootView = inflater.inflate(R.layout.fragment_post, container, false);
//		this.gridViewItems = (GridView) rootView.findViewById(R.id.list_view_post);
//		this.gridViewItems.setOnItemClickListener(this);
//
//		final FloatingActionButton button = (FloatingActionButton) rootView.findViewById(R.id.setter);
//		button.setSize(FloatingActionButton.SIZE_NORMAL);
//		button.setColorNormalResId(R.color.pink);
//		button.setColorPressedResId(R.color.pink_pressed);
//		button.setIcon(R.drawable.ic_pencil_wht_24dp);
//		button.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				final Intent intent = new Intent(SaleItemsFragment.this.getActivity(), DetailActivity.class);
//				Bundle bundle = ActivityBundleFactory.getBundle(null, null, ViewType.CREATE_POST);
//				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
//				SaleItemsFragment.this.startActivity(intent);
//			}
//		});
//
//		//		final FloatingActionButton actionC = new FloatingActionButton(getActivity().getBaseContext());
//		//		((FloatingActionsMenu) findViewById(R.id.multiple_actions)).addButton(actionC);
//		//
//		//		final FloatingActionButton actionA = (FloatingActionButton) findViewById(R.id.action_a);
//		//		actionA.setOnClickListener(new OnClickListener() {
//		//			@Override
//		//			public void onClick(View view) {
//		//				actionA.setTitle("Action A clicked");
//		//			}
//		//		});
//
//
//		this.initialize();
//		return rootView;
//	}

//	private void initialize() {
//		this.gridViewItems.setAdapter(this.postListAdapter);
//	}
	
	@Override
	public void onClick(View view) {
		final Intent intent = new Intent(SearchFragment.this.getActivity(), DetailActivity.class);
		Bundle bundle = ActivityBundleFactory.getBundle(null, null, ViewType.CREATE_POST);
		intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
		SearchFragment.this.startActivity(intent);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		this.onSaleItemClick(adapterView, view, position, id);
	}

	@Override
	protected AbsListView getAbsListView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return (AbsListView) inflater.inflate(R.layout.grid_view_sale_items, null);
	}


    @Override
    protected View getItemView(final int position, final SaleItem saleItem, View convertView, final ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null) {
            final LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.view_sale_items, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textViewSaleItemTitle = (TextView) view.findViewById(R.id.text_view_sale_item_title);
            viewHolder.textViewSaleItemPrice = (TextView) view.findViewById(R.id.text_view_sale_item_price);
            viewHolder.textViewSaleItemModifiedDate = (TextView) view.findViewById(R.id.text_view_sale_item_modified_date);
            viewHolder.viewSeparator = view.findViewById(R.id.view_separator);
            viewHolder.imageViewSaleItemOne = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_one);
            viewHolder.imageViewSaleItemTwo = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_two);
            viewHolder.imageViewSaleItemThree = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_three);
            viewHolder.layout_sale_items_images = (ViewGroup) view.findViewById(R.id.layout_sale_items_images);
            viewHolder.imageButtonContactSeller = (ImageButton) view.findViewById(R.id.image_button_email_seller);
            viewHolder.layoutSideImages = (ViewGroup) view.findViewById(R.id.layout_side_images);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        if (hashSetForOneTimeAnimation.add(position)) {
            view.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_bottom));
        }

        if (saleItem.getSponsored() == SaleItem.SALE_ITEM_SPONSORED) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_sponsored);
            drawable.setBounds(0, 0, SizeUtil.getSizeInPixels(18), SizeUtil.getSizeInPixels(18));
            viewHolder.textViewSaleItemTitle.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            viewHolder.textViewSaleItemTitle.setCompoundDrawablePadding(SizeUtil.getSizeInPixels(4));
        } else {
            viewHolder.textViewSaleItemTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            viewHolder.textViewSaleItemTitle.setCompoundDrawablePadding(0);
        }
        viewHolder.textViewSaleItemTitle.setText(saleItem.getTitle());
        viewHolder.textViewSaleItemPrice.setText(String.format("$ %d", saleItem.getPrice()));

        String categories = getCategoriesText(saleItem);
        if (StringUtil.isNullOrEmpty(categories)) {
            viewHolder.textViewSaleItemModifiedDate.setVisibility(View.GONE);
        } else {
            viewHolder.textViewSaleItemModifiedDate.setVisibility(View.VISIBLE);
            viewHolder.textViewSaleItemModifiedDate.setText(categories);
        }

        ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();

        ArrayList<String> imageIds = saleItem.getImages();
        if (imageIds == null || imageIds.isEmpty()) {
            viewHolder.layout_sale_items_images.setVisibility(View.GONE);
            viewHolder.viewSeparator.setVisibility(View.GONE);
        } else {
            viewHolder.layout_sale_items_images.setVisibility(View.VISIBLE);
            viewHolder.viewSeparator.setVisibility(View.VISIBLE);
            int imagesCount = imageIds.size();
            viewHolder.imageViewSaleItemOne.setVisibility(View.VISIBLE);
            viewHolder.imageViewSaleItemOne.setImageUrl(UrlUtil.getImageUrl(imageIds.get(0)),
                    imageLoader);

            if (imagesCount > 1) {
                viewHolder.layoutSideImages.setVisibility(View.VISIBLE);
                viewHolder.imageViewSaleItemTwo.setVisibility(View.VISIBLE);
                viewHolder.imageViewSaleItemTwo.setImageUrl(UrlUtil.getImageUrl(imageIds.get(1)),
                        imageLoader);
            } else {
                viewHolder.layoutSideImages.setVisibility(View.GONE);
                viewHolder.imageViewSaleItemTwo.setVisibility(View.GONE);
                viewHolder.imageViewSaleItemThree.setVisibility(View.GONE);
            }
            if (imagesCount > 2) {
                viewHolder.layoutSideImages.setVisibility(View.VISIBLE);
                viewHolder.imageViewSaleItemThree.setVisibility(View.VISIBLE);
                viewHolder.imageViewSaleItemThree.setImageUrl(UrlUtil.getImageUrl(imageIds.get(2)),
                        imageLoader);
            } else {
                viewHolder.imageViewSaleItemThree.setVisibility(View.GONE);
            }
        }

        final View currentView = view;
        // enabling item click of grid view

//		// TODO find out better solution than this
        SaleItemViewClickListener listener = new SaleItemViewClickListener((AdapterView<?>) parent, currentView, position);

        view.setOnClickListener(listener);

        // hide mail icon if sale item is created by me
//        if (saleItem.getSeller().getUserId().equals(CurrentUser.getCurrentUser().getUserId())) {
//            viewHolder.imageButtonContactSeller.setVisibility(View.GONE);
//        } else {
//            // hiding/showing emmail id
//            if (StringUtil.isNullOrEmpty(saleItem.getSeller().getEmailId())) {
//                viewHolder.imageButtonContactSeller.setVisibility(View.GONE);
//            } else {
//                viewHolder.imageButtonContactSeller.setVisibility(View.VISIBLE);
//            }
//        }

        viewHolder.imageButtonContactSeller.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getSeller().getUserId(), saleItem.getSeller(), ViewType.MESSAGE_BETWEEN_USER);
                intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
                startActivity(intent);
            }
        });

        return view;
    }
	
	private class SaleItemViewClickListener implements OnClickListener {
		
		private AdapterView<?> parent;
		private View currentView;
		private int position;
				
		public SaleItemViewClickListener(AdapterView<?> parent,
				View currentView, int position) {
			super();
			this.parent = parent;
			this.currentView = currentView;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			onSaleItemClick((AdapterView<?>) parent, currentView, position + 1, currentView.getId());
		}
	}
	
	private void onSaleItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		SaleItem saleItem = listItems.get(position - 1);
		final Intent intent = new Intent(this.getActivity(), DetailActivity.class);
		Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getItemId(), saleItem, ViewType.SALE_ITEM_DETAIL);
		intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
		this.startActivityForResult(intent, REQUEST_CODE_SALE_ITEM_DETAIL);
	}
	
	private static class ViewHolder {
		TextView textViewSaleItemTitle;
		TextView textViewSaleItemPrice;
		TextView textViewSaleItemModifiedDate;
		View viewSeparator;
		ImageButton imageButtonContactSeller;
		NetworkImageView imageViewSaleItemOne;
		NetworkImageView imageViewSaleItemTwo;
		NetworkImageView imageViewSaleItemThree;
		ViewGroup layout_sale_items_images;
		ViewGroup layoutSideImages;
	}

//	@Override
//	protected ArrayList<String> getListItems() {
//		ArrayList<String> list = new ArrayList<>();
//		return null;
//	}
	
//	public enum SaleItemViewType {
//		MY_OPEN_SALE_ITEM(0),
//		MY_PENDING_SALE_ITEM(1),
//		MY_SOLD_SALE_ITEM(2),
//		MY_REJECTED_SALE_ITEM(3),
//		ALL_POSTS(4),
//		ALL_OPEN_SALE_ITEM(5),
//		ALL_PENDING_SALE_ITEM(6),
//		ALL_SOLD_SALE_ITEM(7),
//		ALL_REJECTED_SALE_ITEM(8);
//		
//		private int value;
//		
//		private SaleItemViewType(int value) {
//			this.value = value;
//		}
//		
//		public int getValue() {
//			return value;
//		}
//		
//		public static SaleItemViewType getSaleItemViewType(int value) {
//			SaleItemViewType saleItemViewType = null;
//			for (SaleItemViewType itemViewType : SaleItemViewType.values()) {
//				if (value == itemViewType.value) {
//					saleItemViewType = itemViewType;
//					break;
//				}
//			}
//			return saleItemViewType;
//		}
//		
//		public String getTitle(Context context) {
//			String title = context.getResources().getString(R.string.menu_item_all_posts);
//			switch (this) {
//			case MY_OPEN_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_my_open_posts);
//				break;
//			case MY_PENDING_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_my_pending_posts);
//				break;
//			case MY_REJECTED_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_my_rejected_posts);
//				break;
//			case MY_SOLD_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_my_sold_posts);
//				break;
//			case ALL_OPEN_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_all_posts);
//				break;
//			case ALL_PENDING_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_all_posts);
//				break;
//			case ALL_REJECTED_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_all_posts);
//				break;
//			case ALL_SOLD_SALE_ITEM:
//				title = context.getResources().getString(R.string.menu_item_all_posts);
//				break;
//			default:
//				title = context.getResources().getString(R.string.menu_item_all_posts);
//				break;
//			}
//			return title;
//		}
//	}
	
	private class GetSaleItemResponseHandler extends ApiResponseHandler<SaleItemsResponse> {
		
		@Override
		public void onApiSuccess(SaleItemsResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			searchResultsApiTask = null;
			SearchFragment.this.error = null;
			swipeRefreshLayout.setRefreshing(false);
			
			if (response.getSaleItems() != null && response.getSaleItems().size() > 0) {
				listItems.clear();
				// update data to database
				listItems.addAll(response.getSaleItems());
			}
			update();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			searchResultsApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			SearchFragment.this.error = error;
			update();
		}
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		showView(ContentViewType.CONTENT_LOADING);
		this.getSearchResultsFromApi(true);
	}

	@Override
	protected void displayNoItemsMessage() {
		messageView.showMessage("No Search Items Found", R.drawable.ic_shopping_basket_grey600_48dp, false);
	}
	
	private String getCategoriesText(SaleItem saleItem) {
		StringBuffer text = new StringBuffer();
		if (saleItem.getCategories() != null) {
			for (String categoryNmae : saleItem.getCategories()) {
				text.append(categoryNmae + ", ");
			}
		}
		// remove last ,
		try {
			text.delete(text.length() - 2, text.length() - 1);
		} catch(Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return text.toString().trim();
	}
}
