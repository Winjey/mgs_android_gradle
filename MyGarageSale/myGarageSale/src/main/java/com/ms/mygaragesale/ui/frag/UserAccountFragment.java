package com.ms.mygaragesale.ui.frag;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.response.SuccessResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;
import com.ms.mygaragesale.ui.IActivityEventCallback;
import com.ms.mygaragesale.ui.view.ActionImageLayout;
import com.ms.ui.view.MaterialEditText;

public class UserAccountFragment extends RefreshableFragment implements IActivityEventCallback {
	
	public static final int REQUEST_CODE_USER_DETAIL = 3001;

	private ActionImageLayout image_view_user_image;
	private MaterialEditText edit_text_user_name;
	private EditText edit_text_user_email;
	private EditText edit_text_user_contact;
	private EditText edit_text_user_address;
//	private EditText edit_text_user_location;
//	private Spinner spinner_user_role_type;
//	private TextView text_view_account_status;
	//private Spinner spinner_user_account_status;
	
	private ArrayAdapter<String> arrAdapterAccountType;
	//private ArrayAdapter<String> arrAdapterAccountStatus;
	
	private ApiTask<User> apiTask;
	private User user;
	private boolean isStatusChanged;
	
	public static UserAccountFragment newInstance() {
		final UserAccountFragment fragment = new UserAccountFragment();
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String[] accountTypeArr = {"User", "Moderator", "Admin"};
		arrAdapterAccountType = new ArrayAdapter<>(getActivity(), R.layout.view_spinner_text, accountTypeArr);
		
		//String[] accountStatusArr = {"Pending", "Active", "Blocked"};
		//arrAdapterAccountStatus = new ArrayAdapter<>(getActivity(), R.layout.view_spinner_text, accountStatusArr);
		
		Bundle bundle = getArguments();
		if (bundle == null) {
			throw new IllegalArgumentException("Intent does not contain any bundle data with key");
		}
		Object object = null;
		String userId = null;
		if (bundle != null && bundle.containsKey(ActivityBundleFactory.KEY_USER_OBJECT) && 
				(object = bundle.getSerializable(ActivityBundleFactory.KEY_USER_OBJECT)) != null) { 
			this.user = (User) object;
		} else if (bundle.containsKey(ActivityBundleFactory.KEY_USER_ID) && 
				(userId = bundle.getString(ActivityBundleFactory.KEY_USER_ID, null)) != null) {
			this.user = new User();
			this.user.setUserId(userId);
			this.getUserData();
		} else {
			throw new IllegalArgumentException("Bundle does not contains any user object.");
		}
	}
	
	@Override
	protected void initialize() {
		this.initView();
		this.update();
	}
	
	private void initView() {
		//this.hideFloatingActionButton();
		this.swipeRefreshLayout.setEnabled(false);
		this.setViewsEnabled(false);
//		spinner_user_role_type.setAdapter(arrAdapterAccountType);
		//spinner_user_account_status.setAdapter(arrAdapterAccountStatus);
	}
	
	private void update() {
		showView(ContentViewType.CONTENT_DATA);
		edit_text_user_name.setText(this.user.getName());
		edit_text_user_email.setText(this.user.getEmailId());
		edit_text_user_contact.setText(this.user.getMobileNumber());
		edit_text_user_address.setText(this.user.getAddress());
//		edit_text_user_location.setText(this.user.getLocation());
//		spinner_user_role_type.setSelection(getSpinnerIndexForUserRole(this.user.getUserRole()));
		//spinner_user_account_status.setSelection(getSpinnerIndexForAccountStatus(this.user.getAccountStatus()));
		
//		setAccountStatusText();
		
		String imageId = user.getImageId();
		if (!StringUtil.isNullOrEmpty(imageId)) {
			VolleyRequestSingleton requestSingleton = VolleyRequestSingleton.getInstance(getActivity());
			ImageLoader imageLoader = requestSingleton.getImageLoader();
			imageLoader.get(UrlUtil.getImageUrl(imageId), ImageLoader.getImageListener(image_view_user_image.getImageView(),
			         R.drawable.ic_anonymous_avatar_40dp, R.drawable.ic_anonymous_avatar_40dp));
		} 
	}
	
//	private void setAccountStatusText() {
//		// if current user role is user, does not display account status
//		if (CurrentUser.getCurrentUser().getUserRole() == User.USER_ROLE_USER) {
//			text_view_account_status.setVisibility(View.GONE);
//			return;
//		}
//		// if current user role is admin/moderator and upgradation is pending, does not display account status
//		if ((CurrentUser.getCurrentUser().getUserRole() == User.USER_ROLE_MODERATOR
//				|| CurrentUser.getCurrentUser().getUserRole() == User.USER_ROLE_ADMIN)
//				&& CurrentUser.getCurrentUser().getAccountStatus() == User.ACCOUNT_STATUS_UPGRADE_PENDING) {
//			text_view_account_status.setVisibility(View.GONE);
//			return;
//		}
//
//		int color = 0;
//		String text = null;
//		int accountStatus = user.getAccountStatus();
//		if (accountStatus == User.ACCOUNT_STATUS_ACTIVE) {
//			text =  "Account Activated";
//			color = getResources().getColor(R.color.green_material_color);
//		} else if (accountStatus == User.ACCOUNT_STATUS_BLOCKED) {
//			text =  "Account Blocked";
//			color = getResources().getColor(R.color.red_material_color);
//		} else if (accountStatus == User.ACCOUNT_STATUS_PENDING) {
//			text =  "Account Activation Pending";
//			color = getResources().getColor(R.color.yellow_material_color);
//		} else if (accountStatus == User.ACCOUNT_STATUS_UPGRADE_PENDING) {
//			text =  "Account Upgradation Pending";
//			color = getResources().getColor(R.color.orange_material_color);
//		} else if (accountStatus == User.ACCOUNT_STATUS_DOWNGRADE_PENDING) {
//			text =  "Account Downgradation Pending";
//			color = getResources().getColor(R.color.orange_material_color);
//		}
//		text_view_account_status.setText(text);
//		text_view_account_status.setTextColor(color);
//	}
	
	
//	private int getSpinnerIndexForAccountStatus(int accountStatus) {
//		if (accountStatus == User.ACCOUNT_STATUS_PENDING) {
//			return 0;
//		}
//		if (accountStatus == User.ACCOUNT_STATUS_ACTIVE) {
//			return 1;
//		}
//		if (accountStatus == User.ACCOUNT_STATUS_BLOCKED) {
//			return 2;
//		}
//		return 0;
//	}
	
	private int getSpinnerIndexForUserRole(int userRole) {
		if (userRole == User.USER_ROLE_USER) {
			return 0;
		}
		if (userRole == User.USER_ROLE_MODERATOR) {
			return 1;
		}
		if (userRole == User.USER_ROLE_ADMIN) {
			return 2;
		}
		return 0;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final ActionBar actionBar = ((ActionBarActivity) this.getActivity()).getSupportActionBar();
		String tiitle = "User Account";
		if (!StringUtil.isNullOrEmpty(user.getName())) {
			tiitle = user.getName();
		}
		actionBar.setTitle(tiitle);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.user_account, menu);
		
		if (CurrentUser.getCurrentUser().getUserRole() != CurrentUser.USER_ROLE_ADMIN 
				&& CurrentUser.getCurrentUser().getUserRole() != CurrentUser.USER_ROLE_ADMIN) {
			menu.removeItem(R.id.action_activate_account);
			menu.removeItem(R.id.action_upgrade_account);
			menu.removeItem(R.id.action_block_account);
		}
		
		if (user.getAccountStatus() == User.ACCOUNT_STATUS_ACTIVE || user.getAccountStatus() == User.ACCOUNT_STATUS_UPGRADE_PENDING) {
			menu.removeItem(R.id.action_activate_account);
		}
		
		if (user.getAccountStatus() == User.ACCOUNT_STATUS_BLOCKED) {
			menu.removeItem(R.id.action_activate_sale_item);
		}
		
		if (user.getAccountStatus() != User.ACCOUNT_STATUS_UPGRADE_PENDING) {
			menu.removeItem(R.id.action_upgrade_account);
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result =  super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.action_send_message:
			sendMessage();
			break;
		case R.id.action_activate_account:
			activateUser();
			break;
		case R.id.action_upgrade_account:
			upgradeUser();
			break;
		case R.id.action_block_account:
			blockUser();
			break;
		}
		return result;
	}
	
	private void sendMessage() {
		Intent intent = new Intent(getActivity(), DetailActivity.class);
		Bundle bundle = ActivityBundleFactory.getBundle(user.getUserId(), user, ViewType.MESSAGE_BETWEEN_USER);
		intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
		startActivity(intent);
	}
	
	private void activateUser() {
		String title = "Activate User";
		String message = "Do you want to activate this user account?";
		String positiveButton = "ACTIVATE";
		String url = UrlUtil.getActivateUserUrl(user.getUserId());
		showConfirmationDialog(title, message, positiveButton, url, UserActionType.ACTIVATE);
	}
	
	private void upgradeUser() {
		String title = "Upgrade User";
		String message = "Do you want to upgrade this user account?";
		String positiveButton = "UPGRADE";
		String url = UrlUtil.getActivateUserUrl(user.getUserId());
		showConfirmationDialog(title, message, positiveButton, url, UserActionType.UPGRADE);
	}

	private void blockUser() {
		String title = "Block User";
		String message = "Do you want to block this user account?";
		String positiveButton = "BLOCK";
		String url = UrlUtil.getBlockUserUrl(user.getUserId());
		showConfirmationDialog(title, message, positiveButton, url, UserActionType.BLOCK);
	}
		
	private void showConfirmationDialog(final String title, final String message, final String positiveButton, final String url, 
			final UserActionType actionType) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ApiTask<SuccessResponse> apiTask = new ApiTaskFactory<SuccessResponse>().newInstance(getActivity(), 
						url, APIMethod.GET, 
							SuccessResponse.class, new UserActionResponseHandler(actionType));
				apiTask.executeRequest(null, false);
				MGSApplication.showToast(getStartMessage(actionType), Toast.LENGTH_SHORT);
			}
		});
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
			
	}
	
	private enum UserActionType {
		ACTIVATE, BLOCK, UPGRADE
	}
	
	private String getStartMessage(UserActionType actionType) {
		switch (actionType) {
			case ACTIVATE:
				return "Activating Account";
			case BLOCK:
				return "Blocking Account";
			case UPGRADE:
				return "Upgrading Account";
		}
		return "";
	}
	
	private String getSuccessMessage(UserActionType actionType) {
		switch (actionType) {
			case ACTIVATE:
				return "User account is activated.";
			case BLOCK:
				return "User account is blocked.";
			case UPGRADE:
				return "User account is Upgraded.";
		}
		return "";
	}
	
	private class UserActionResponseHandler extends ApiResponseHandler<SuccessResponse> {
		
		private UserActionType actionType;
		
		public UserActionResponseHandler(UserActionType actionType) {
			this.actionType = actionType;
		}
		
		@Override
		public void onApiSuccess(SuccessResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			isStatusChanged = true;
			MGSApplication.showToast(getSuccessMessage(actionType), Toast.LENGTH_LONG);
			if (actionType == UserActionType.ACTIVATE || actionType == UserActionType.UPGRADE) {
				user.setAccountStatus(User.ACCOUNT_STATUS_ACTIVE);
			} else if (actionType == UserActionType.BLOCK) {
				user.setAccountStatus(User.ACCOUNT_STATUS_BLOCKED);
			}
			getActivity().invalidateOptionsMenu();
//			setAccountStatusText();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
		}
	}
	
	private void setViewsEnabled(boolean enable) {
		this.image_view_user_image.setActionButtonEnabled(enable);
		this.edit_text_user_name.setEnabled(enable);
		this.edit_text_user_email.setEnabled(enable);
		this.edit_text_user_contact.setEnabled(enable);
		this.edit_text_user_address.setEnabled(enable);
//		this.edit_text_user_location.setEnabled(enable);
//		this.spinner_user_role_type.setEnabled(enable);
		//this.spinner_user_account_status.setEnabled(enable);
	}
	
	private void getUserData() {
		apiTask = new ApiTaskFactory<User>().newInstance(getActivity(), UrlUtil.getUserUrl(String.valueOf(user.getUserId())), 
				APIMethod.GET, User.class, new GetUserResponse());
		apiTask.executeRequest(user, false);
	}
	
	private class GetUserResponse extends ApiResponseHandler<User> {
		@Override
		public void onApiSuccess(User response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			user = response;
			update();
		}

		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			Logger.log("image upload fail with error " + error.getMesssage());
			// TODO dialog or toast
		}
	}

	@Override
	protected View getContentView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.fragment_user, null);
		this.image_view_user_image = (ActionImageLayout) view.findViewById(R.id.image_view_user_image);
		this.edit_text_user_name = (MaterialEditText) view.findViewById(R.id.edit_text_user_name);
		this.edit_text_user_email = (EditText) view.findViewById(R.id.edit_text_user_email);
		this.edit_text_user_contact = (EditText) view.findViewById(R.id.edit_text_user_contact);
		this.edit_text_user_address = (EditText) view.findViewById(R.id.edit_text_user_address);
//		this.edit_text_user_location = (EditText) view.findViewById(R.id.edit_text_user_location);
//		this.spinner_user_role_type = (Spinner) view.findViewById(R.id.spinner_user_account_type);
//		this.text_view_account_status = (TextView) view.findViewById(R.id.text_view_account_status);
		//this.spinner_user_account_status = (Spinner) view.findViewById(R.id.spinner_user_account_status);
		return view;
	}

	@Override
	public boolean onActivityBackPressed() {
		if (isStatusChanged) {
			getActivity().setResult(Activity.RESULT_OK);
		}
		this.getActivity().finish();
		return true;
	}
	
}
