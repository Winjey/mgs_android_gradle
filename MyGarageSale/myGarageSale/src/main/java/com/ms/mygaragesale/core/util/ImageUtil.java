package com.ms.mygaragesale.core.util;

import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.media.ExifInterface;

import com.ms.mygaragesale.core.DefaultConf;
import com.ms.mygaragesale.core.Logger;

public class ImageUtil {

	String TAG = "debug_imageUtil";

	public static Bitmap getRoundedShape(int width, int height,
			Bitmap scaleBitmapImage) {
		// TODO Auto-generated method stub
		int targetWidth = width;
		int targetHeight = height;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				Math.min((float) targetWidth, (float) targetHeight) / 2,
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}

	// public Bitmap getBitmap(Context context, String path) {
	//
	// Uri uri = Uri.parse(path);
	// InputStream in = null;
	// try {
	// final int IMAGE_MAX_SIZE = 400000; // 400KB
	// in = context.getContentResolver().openInputStream(uri);
	//
	// // Decode image size
	// BitmapFactory.Options o = new BitmapFactory.Options();
	// o.inJustDecodeBounds = true;
	// BitmapFactory.decodeStream(in, null, o);
	// in.close();
	//
	// int scale = 1;
	// while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
	// IMAGE_MAX_SIZE) {
	// scale++;
	// }
	// Log.d(TAG, "scale = " + scale + ", orig-width: " + o.outWidth +
	// ", orig-height: " + o.outHeight);
	//
	// Bitmap b = null;
	// in = context.getContentResolver().openInputStream(uri);
	// if (scale > 1) {
	// scale--;
	// // scale to max possible inSampleSize that still yields an image
	// // larger than target
	// o = new BitmapFactory.Options();
	// o.inSampleSize = scale;
	// b = BitmapFactory.decodeStream(in, null, o);
	//
	// // resize to desired dimensions
	// int height = b.getHeight();
	// int width = b.getWidth();
	// Log.d(TAG, "1th scale operation dimenions - width: " + width +
	// ",height: " + height);
	//
	// double y = Math.sqrt(IMAGE_MAX_SIZE
	// / (((double) width) / height));
	// double x = (y / height) * width;
	//
	// Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
	// (int) y, true);
	// b.recycle();
	// b = scaledBitmap;
	//
	// System.gc();
	// } else {
	// b = BitmapFactory.decodeStream(in);
	// }
	// in.close();
	//
	// Log.d(TAG, "bitmap size - width: " +b.getWidth() + ", height: " +
	// b.getHeight());
	// return b;
	// } catch (IOException e) {
	// Log.e(TAG, e.getMessage(),e);
	// return null;
	// }
	// }
	//
	// public static void decodeFile(String url) throws Exception {
	// Bitmap b = null;
	//
	// //Decode image size
	// BitmapFactory.Options o = new BitmapFactory.Options();
	// o.inJustDecodeBounds = true;
	//
	// File f = new File(url);
	// FileInputStream fis = new FileInputStream(f);
	//
	// BitmapFactory.decodeStream(fis, null, o);
	// fis.close();
	//
	//
	// int IMAGE_MAX_SIZE = 900;
	//
	// int scale = 1;
	// if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
	// scale = (int)Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
	// (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
	// }
	//
	// Logger.log("scale: " + scale);
	// //Decode with inSampleSize
	// BitmapFactory.Options o2 = new BitmapFactory.Options();
	// o2.inJustDecodeBounds = false;
	// o2.inSampleSize = scale;
	// // FileInputStream fis = null;
	// fis = new FileInputStream(f);
	//
	//
	// b = BitmapFactory.decodeStream(fis, null, o2);
	// fis.close();
	//
	//
	// OutputStream outputStream = null;
	// outputStream = new FileOutputStream(f);
	//
	// b.compress(CompressFormat.PNG, 80, outputStream);
	// }
	//
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;
			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		return inSampleSize;
	}

	public static String compressImage(String imageUri) {
		String outputUri = null;
		try {
			Bitmap scaledBitmap = null;
			BitmapFactory.Options options = new BitmapFactory.Options();
	
			// by setting this field as true, the actual bitmap pixels are not
			// loaded in the memory. Just the bounds are loaded. If
			// you try the use the bitmap here, you will get null.
			options.inJustDecodeBounds = true;
			Bitmap inBitmap = BitmapFactory.decodeFile(imageUri, options);
			
			int actualHeight = options.outHeight;
			int actualWidth = options.outWidth;
	
			float maxHeight = DefaultConf.IMAGE_MAX_HEIGHT;
			float maxWidth = DefaultConf.IMAGE_MAX_WIDTH;
			float imgRatio = actualWidth / actualHeight;
			float maxRatio = maxWidth / maxHeight;
	
			// width and height values are set maintaining the aspect ratio of the image
			if (actualHeight > maxHeight || actualWidth > maxWidth) {
				if (imgRatio < maxRatio) {
					imgRatio = maxHeight / actualHeight;
					actualWidth = (int) (imgRatio * actualWidth);
					actualHeight = (int) maxHeight;
				} else if (imgRatio > maxRatio) {
					imgRatio = maxWidth / actualWidth;
					actualHeight = (int) (imgRatio * actualHeight);
					actualWidth = (int) maxWidth;
				} else {
					actualHeight = (int) maxHeight;
					actualWidth = (int) maxWidth;
				}
			}
	
			// setting inSampleSize value allows to load a scaled down version of the original image
			options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
	
			// inJustDecodeBounds set to false to load the actual bitmap
			options.inJustDecodeBounds = false;
	
			// this options allow android to claim the bitmap memory if it runs low on memory
			options.inPurgeable = true;
			options.inInputShareable = true;
			options.inTempStorage = new byte[16 * 1024];
	
			// load the bitmap from its path
			inBitmap = BitmapFactory.decodeFile(imageUri, options);
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
	
			float ratioX = actualWidth / (float) options.outWidth;
			float ratioY = actualHeight / (float) options.outHeight;
			float middleX = actualWidth / 2.0f;
			float middleY = actualHeight / 2.0f;
	
			Matrix scaleMatrix = new Matrix();
			scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
	
			Canvas canvas = new Canvas(scaledBitmap);
			canvas.setMatrix(scaleMatrix);
			canvas.drawBitmap(inBitmap, middleX - inBitmap.getWidth() / 2,
					middleY - inBitmap.getHeight() / 2, new Paint(
							Paint.FILTER_BITMAP_FLAG));
	
			// check the rotation of the image and display it properly
			ExifInterface exif = new ExifInterface(imageUri);
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
			} else if (orientation == 3) {
				matrix.postRotate(180);
			} else if (orientation == 8) {
				matrix.postRotate(270);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), 
						scaledBitmap.getHeight(), matrix, true);
	
			FileOutputStream outStream = null;
			File file = new File(imageUri);
			outStream = new FileOutputStream(file);
			// write the compressed bitmap at the destination specified by filename.
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
			outputUri = file.getAbsolutePath();
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		} catch (OutOfMemoryError e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}

		return outputUri;
	}
}
