package com.ms.mygaragesale.ui.frag;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.LoginButton;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.IntentRequestCode;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.google.GLocation;
import com.ms.mygaragesale.ui.MainActivity;
import com.ms.mygaragesale.ui.frag.LoadingMessageDialogFragment.DialogFragmentCompleteListener;
import com.ms.mygaragesale.ui.view.FbLoginButtonEx;
import com.ms.mygaragesale.ui.view.LoadingView;
import com.ms.mygaragesale.ui.view.LocationPredictionView;
import com.ms.mygaragesale.ui.view.MessageView;
import com.ms.mygaragesale.ui.view.ViewFlipperEx;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

public class AuthenticationFragment extends Fragment implements
		OnClickListener, DialogFragmentCompleteListener {

	private static final int REAUTH_ACTIVITY_CODE = 1500; 
	private static final String[] READ_PERMISSIONS = {"public_profile", "email", "user_location"/*, "user_groups"*/};
	private static final String WRITE_PERMISSION = "publish_actions";
	
	private ViewFlipperEx viewFlipperEx;
	private MessageView messageView;
	private LoadingView loadingView;

	private ViewGroup layoutSignup;
	private ViewGroup layoutLogin;
	private TwitterLoginButton buttonTwitterLogin;
	private FbLoginButtonEx buttonFacebookLogin;
	private Button buttonLogin;
	private Button buttonSignUp;
	private Button buttonLoginView;
	private Button buttonSignUpView;
	private EditText editTextUsername;
	private EditText editTextPassword;
	private EditText editTextNewEmailId;
	private EditText editTextNewPassword;
	private EditText editTextNewName;
	private EditText editTextNewMobileNumber;
	private LocationPredictionView locationPredictionView;

	private AuthFragContentViewType currentViewType;

	public static AuthenticationFragment newInstance() {
		final AuthenticationFragment fragment = new AuthenticationFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRetainInstance(true);
		this.currentViewType = AuthFragContentViewType.LOGIN;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		updateReferenceToAuthDialog(false);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == IntentRequestCode.TWITTER) {
				// Pass the activity result to the login button.
				buttonTwitterLogin.onActivityResult(requestCode, resultCode,
						data);
			} else if (requestCode == IntentRequestCode.FACEBOOK) {
				final Session session = Session.getActiveSession();
				session.onActivityResult(this.getActivity(), requestCode,
						resultCode, data);
				if (session.getState() == SessionState.CLOSED_LOGIN_FAILED) {
					// TODO login failed
					return;
				}
                // disabling request for "publish actions" permission to post to facebook
//				this.requestPublishPermissions(session);
//
//			} else if (requestCode == REAUTH_ACTIVITY_CODE) {
				// delete old user if present any
				CurrentUser oldUser = CurrentUser.getCurrentUser();
				oldUser.deleteCurrentUser();
		
				CurrentUser currentUser = CurrentUser.getCurrentUser();
				currentUser.setAccountType(User.ACCOUNT_TYPE_FACEBOOK);
				currentUser.updateCurrentUser();
				showAuthDialog(AuthenticationDialogFragment.DO_COMPLETE_PROCESS);
			}
		} else {
			if (requestCode == IntentRequestCode.TWITTER) {
				if (getActivity() != null) {
					MGSApplication.showToast("Unable to login using twitter. Some error occurred. Please try again later.", Toast.LENGTH_LONG);
				}
			} else if (requestCode == IntentRequestCode.FACEBOOK) {
				if (getActivity() != null) {
					Toast.makeText(getActivity(), "Unable to login using facebook. Some error occurred. Please try again later.", Toast.LENGTH_LONG).show();
				}
			}
		}
	}
	
	private void requestPublishPermissions(Session session) {
	    if (session != null) {
	        Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this.getActivity(), WRITE_PERMISSION);

	        newPermissionsRequest.setRequestCode(REAUTH_ACTIVITY_CODE);
	        Session mSession = Session.openActiveSessionFromCache(this.getActivity());
	        mSession.requestNewPublishPermissions(newPermissionsRequest);
	    }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_authentication, container, false);
		viewFlipperEx = (ViewFlipperEx) rootView.findViewById(R.id.view_flipper);
		loadingView = (LoadingView) rootView.findViewById(R.id.loading_view);
		messageView = (MessageView) rootView.findViewById(R.id.message_view);

		ViewGroup viewGroupLogin = (ViewGroup) rootView.findViewById(R.id.layout_login);
		layoutLogin = (ViewGroup) rootView.findViewById(R.id.layout_login_inner);
		buttonLogin = (Button) viewGroupLogin.findViewById(R.id.button_login);
		buttonSignUpView = (Button) viewGroupLogin.findViewById(R.id.button_signup_view);
		buttonFacebookLogin = (FbLoginButtonEx) viewGroupLogin.findViewById(R.id.button_login_facebook);
		buttonTwitterLogin = (TwitterLoginButton) viewGroupLogin.findViewById(R.id.button_login_twitter);
		editTextUsername = (EditText) viewGroupLogin.findViewById(R.id.edit_text_username);
		editTextPassword = (EditText) viewGroupLogin.findViewById(R.id.edit_text_password);

		ViewGroup viewGroupSignup = (ViewGroup) rootView.findViewById(R.id.layout_signup);
		layoutSignup = (ViewGroup) rootView.findViewById(R.id.layout_signup_inner);
		buttonLoginView = (Button) viewGroupSignup.findViewById(R.id.button_login_view);
		buttonSignUp = (Button) viewGroupSignup.findViewById(R.id.button_signup);
		editTextNewEmailId = (EditText) viewGroupSignup.findViewById(R.id.edit_text_new_email_id);
		editTextNewPassword = (EditText) viewGroupSignup.findViewById(R.id.edit_text_new_password);
		editTextNewName = (EditText) viewGroupSignup.findViewById(R.id.edit_text_new_name);
		editTextNewMobileNumber = (EditText) viewGroupSignup.findViewById(R.id.edit_text_new_mobile_number);
		locationPredictionView = (LocationPredictionView) viewGroupSignup.findViewById(R.id.location_prediction_view);

		initView();
		update();
		return rootView;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		this.updateAuthViewWidth(newConfig);

	}
	
	@Override
	public void onClick(View view) {
		if (view == buttonLogin) {
			// delete old user if present any
			CurrentUser oldUser = CurrentUser.getCurrentUser();
			oldUser.deleteCurrentUser();
			
			doLogin();
		}
		if (view == buttonLoginView) {
			showView(AuthFragContentViewType.LOGIN);
		}
		if (view == buttonSignUpView) {
			showView(AuthFragContentViewType.SIGNUP);
		}
		if (view == buttonSignUp) {
			// delete old user if present any
			CurrentUser oldUser = CurrentUser.getCurrentUser();
			oldUser.deleteCurrentUser();
			
			doSignUp();
		}
	}
	
	@Override
	public void onComplete(MGSError error) {
		// success fully open main activity
		if (error == null) {
			Logger.log("Login success");
			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			getActivity().finish();
		} 
	}
	
	private void initView() {
		// update twitter button based on our ui
		buttonTwitterLogin.setBackgroundResource(R.drawable.selector_twitter_login_button);
		buttonTwitterLogin.setTextAppearance(getActivity(), R.style.twitter_button);
		buttonTwitterLogin.setText("LOGIN WITH TWITTER");

		showView(this.currentViewType);
		this.updateAuthViewWidth(getResources().getConfiguration());
		editTextNewName.requestFocus();
		
		buttonFacebookLogin.setReadPermissions(READ_PERMISSIONS);

		buttonTwitterLogin.setCallback(new Callback<TwitterSession>() {

			@Override
			public void success(Result<TwitterSession> result) {
				Logger.log("twitter login success");
				// get email of user
				// TwitterAuthClient authClient = new TwitterAuthClient();
				// authClient.requestEmail(result.data, new Callback<String>() {
				// @Override
				// public void success(Result<String> result) {
				// System.out.println(result);
				// }
				//
				// @Override
				// public void failure(TwitterException exception) {
				// System.out.println(exception);
				// // user does not have mail address, unable to login using
				// twitter
				// }
				// });
				
				// delete old user if present any
				CurrentUser oldUser = CurrentUser.getCurrentUser();
				oldUser.deleteCurrentUser();
				
				CurrentUser currentUser = CurrentUser.getCurrentUser();
				currentUser.setAccountType(User.ACCOUNT_TYPE_TWITTER);
				currentUser.updateCurrentUser();
				showAuthDialog(AuthenticationDialogFragment.DO_COMPLETE_PROCESS);
			}

			@Override
			public void failure(TwitterException exception) {
				// Do something on failure
				// TODO unable to logged in using Twitter, user decline
				// display error
				MGSApplication.showToast(exception.getMessage(), Toast.LENGTH_LONG);
			}
		});

		buttonLogin.setOnClickListener(this);
		buttonSignUp.setOnClickListener(this);
		if (buttonLoginView != null) {
			buttonLoginView.setOnClickListener(this);
		}
		if (buttonSignUpView != null) {
			buttonSignUpView.setOnClickListener(this);
		}
	}

	private void updateAuthViewWidth(Configuration newConfig) {
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			if (layoutSignup != null) {
				ViewGroup.LayoutParams params = layoutSignup.getLayoutParams();
				params.width = (int) getResources().getDimension(R.dimen.authentication_view_width_land);
				layoutSignup.requestLayout();
			}
			if (layoutLogin != null) {
				ViewGroup.LayoutParams params = layoutLogin.getLayoutParams();
				params.width = (int) getResources().getDimension(R.dimen.authentication_view_width_land);
				layoutLogin.requestLayout();
			}
		} else {
			if (layoutSignup != null) {
				ViewGroup.LayoutParams params = layoutSignup.getLayoutParams();
				params.width = (int) getResources().getDimension(R.dimen.authentication_view_width_port);
				layoutSignup.requestLayout();
			}
			if (layoutLogin != null) {
				ViewGroup.LayoutParams params = layoutLogin.getLayoutParams();
				params.width = (int) getResources().getDimension(R.dimen.authentication_view_width_port);
				layoutLogin.requestLayout();
			}
		}
	}
	
	private void update() {
//		if (error != null) {
//			this.messageView.showMessage(error);
//			showView(ContentViewType.MESSAGE);
//		}
	}

	// private void blur(Bitmap bitmap, View view, float radius) {
	// Bitmap overlay = Bitmap.createBitmap(view.getMeasuredWidth(),
	// view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
	// Canvas canvas = new Canvas(overlay);
	// canvas.drawBitmap(bitmap, -view.getLeft(), -view.getTop(), null);
	// RenderScript renderScript = RenderScript.create(getActivity());
	//
	// Allocation overlayAlloc = Allocation.createFromBitmap(renderScript,
	// overlay);
	// ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
	// overlayAlloc.getElement());
	// blur.setInput(overlayAlloc);
	// blur.setRadius(radius);
	// blur.forEach(overlayAlloc);
	// overlayAlloc.copyTo(overlay);
	// view.setBackground(new BitmapDrawable(getResources(), overlay));
	// renderScript.destroy();
	// }

	

//	@Override
//	public void onSuccess() {
//		// success fully open main activity
//		Logger.log("Login success");
//		Intent intent = new Intent(getActivity(), MainActivity.class);
//		startActivity(intent);
//		getActivity().finish();
//	}
//
//	@Override
//	public void onFailure(MGSError error) {
//		// display error message
//		showView(ContentViewType.MESSAGE);
//		this.error = error;
//		this.messageView.showMessage(error);
//		Logger.log(error.getMesssage());
//	}
//
//	@Override
//	public void onMessage(String message) {
//		// display message on dialog box
////		showView(ViewType.LOADING);
////		Logger.log(message);
//	}

	private void showView(AuthFragContentViewType viewType) {
		this.currentViewType = viewType;
		this.viewFlipperEx.setDisplayedChild(viewType.ordinal());
//		Rotate3dAnimation outAnim = new Rotate3dAnimation(0, 0, 360, 270, 0, 0);
//		outAnim.setDuration(200);
//		Rotate3dAnimation inAnim = new Rotate3dAnimation(0, 0, 0, 90, 0, 0);
//		inAnim.setDuration(150);
		Animation inAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
		this.viewFlipperEx.setInAnimation(inAnim);
		//this.viewFlipperEx.setOutAnimation(outAnim);
	}

	

	

	private void doLogin() {
		// do login with credentials
		// verify input values
		
		// delete old user if present any
		CurrentUser oldUser = CurrentUser.getCurrentUser();
		oldUser.deleteCurrentUser();
		
		
		if (this.verifyLoginInput()) {
			CurrentUser currentUser = CurrentUser.getCurrentUser();
			currentUser.setUsername(editTextUsername.getText().toString());
			currentUser.setPassword(editTextPassword.getText().toString());
			showAuthDialog(AuthenticationDialogFragment.DO_LOGIN);
		}
	}

	private void doSignUp() {
		// do signup with credentials
		// verify input values
		
		// delete old user if present any
		CurrentUser oldUser = CurrentUser.getCurrentUser();
		oldUser.deleteCurrentUser();
		
		if (this.verifySignUpInput()) {
			CurrentUser user = CurrentUser.getCurrentUser();
			user.setUsername(editTextNewEmailId.getText().toString());
			user.setPassword(editTextNewPassword.getText().toString());
			user.setName(editTextNewName.getText().toString());
			user.setEmailId(editTextNewEmailId.getText().toString());
			user.setMobileNumber(editTextNewMobileNumber.getText().toString());
			user.setLocation(new GLocation(locationPredictionView.getPlace()));
			user.setAccountType(User.ACCOUNT_TYPE_SERVER);

			//showView(ContentViewType.LOADING);
			showAuthDialog(AuthenticationDialogFragment.DO_SIGNUP);
		}
	}

	private boolean verifyLoginInput() {
		if (StringUtil.isNullOrEmpty(editTextUsername.getText().toString()) 
				|| StringUtil.isNullOrEmpty(editTextPassword.getText().toString())) {
			MGSApplication.showToast("Invalid inputs.", Toast.LENGTH_LONG);
			return false;
		}
		return true;
	}

	private boolean verifySignUpInput() {
		if (StringUtil.isNullOrEmpty(editTextNewEmailId.getText().toString())) {
			MGSApplication.showToast("Email id require.", Toast.LENGTH_LONG);
			return false;
		}
		if (StringUtil.isNullOrEmpty(editTextNewPassword.getText().toString())) {
			MGSApplication.showToast("Password require.", Toast.LENGTH_LONG);
			return false;
		}
		if (StringUtil.isNullOrEmpty(editTextNewName.getText().toString())) {
			MGSApplication.showToast("Name require.", Toast.LENGTH_LONG);
			return false;
		}
		if (StringUtil.isNullOrEmpty(locationPredictionView.getPlace().getDescription())) {
			MGSApplication.showToast("Location require.", Toast.LENGTH_LONG);
			return false;
		}
		return true;
	}

	private void updateReferenceToAuthDialog(boolean enable) {
		Fragment prev = getFragmentManager().findFragmentByTag("dialog");
	    if (prev != null && prev instanceof AuthenticationDialogFragment) {
	    	DialogFragmentCompleteListener listener = this;
	    	if (!enable) {
				listener = null;
			}
	    	((AuthenticationDialogFragment) prev).setDialogFragmentCompleteListener(listener);
	    }
	}
	
	private void showAuthDialog(int authProcess) {
		AuthenticationDialogFragment authenticationDialogFragment = AuthenticationDialogFragment.newInstance(authProcess);
		authenticationDialogFragment.setDialogFragmentCompleteListener(this);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment prev = fragmentManager.findFragmentByTag("dialog");
	    if (prev != null) {
	    	transaction.remove(prev);
	    }
	    transaction.addToBackStack(null);
	    authenticationDialogFragment.show(transaction, "dialog");
	}

	private enum AuthFragContentViewType {
		//LOADING, MESSAGE, 
		LOGIN, SIGNUP
	}

}
