package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.ImageUtil;
import com.ms.mygaragesale.core.util.LogoutUtility;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.MenuItem;
import com.ms.mygaragesale.model.MenuItemChild;
import com.ms.mygaragesale.model.MenuItemGroup;
import com.ms.mygaragesale.ui.SearchActivity;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 */
public class NavigationDrawerFragment extends Fragment implements OnGroupClickListener, OnChildClickListener {

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_GROUP_SELECTED_POSITION = "selected_group_in_navigation_drawer_position";
	private static final String STATE_CHILD_SELECTED_POSITION = "selected_child_in_navigation_drawer_position";

//	/**
//	 * Per the design guidelines, you should show the drawer on launch until the user manually
//	 * expands it. This shared preference tracks this.
//	 */
//	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */
	private NavigationDrawerCallbacks mCallbacks;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawerLayout;
	private ExpandableListView mDrawerListView;
	private View mFragmentContainerView;

	private MenuItemAdapter mMenuItemAdapter;

	private ArrayList<MenuItemGroup> listMenuItemGroups;
	private Map<MenuItemGroup, ArrayList<MenuItemChild>> mMapMenuItems;
	private int mCurrentSelectedGroupPosition;
	private int mCurrentSelectedChildPosition;
	private boolean mFromSavedInstanceState;
//	private boolean mUserLearnedDrawer;

	public NavigationDrawerFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		this.listMenuItemGroups = MenuItem.getMenuItemGroupsList();
		this.mMapMenuItems = MenuItem.getMenuItemsMap();
		this.mMenuItemAdapter = new MenuItemAdapter();
		// Read in the flag indicating whether or not the user has demonstrated awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
//		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
//		this.mUserLearnedDrawer = sp.getBoolean(NavigationDrawerFragment.PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			this.mCurrentSelectedGroupPosition = savedInstanceState.getInt(NavigationDrawerFragment.STATE_GROUP_SELECTED_POSITION);
			this.mCurrentSelectedChildPosition = savedInstanceState.getInt(NavigationDrawerFragment.STATE_CHILD_SELECTED_POSITION);
			this.mFromSavedInstanceState = true;
		} else {
			this.mCurrentSelectedGroupPosition = listMenuItemGroups.indexOf(MenuItemGroup.ALL_POSTS);
			this.mCurrentSelectedChildPosition = 0;
			this.mFromSavedInstanceState = false;
		}
	}

	@Override
	public void onActivityCreated (Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of actions in the action bar.
		this.setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
		this.mDrawerListView = (ExpandableListView) view.findViewById(R.id.ex_list_view_menu_items);
		this.mDrawerListView.setOnChildClickListener(this);
		this.mDrawerListView.setOnGroupClickListener(this);

		this.mDrawerListView.setAdapter(this.mMenuItemAdapter);
		for(int i = 0; i < this.listMenuItemGroups.size(); i++) {
			this.mDrawerListView.expandGroup(i);
		}
		//this.mDrawerListView.setItemChecked(this.mCurrentSelectedGroupPosition, true);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		// Select either the default item or the last selected item.
		this.selectItem(this.mCurrentSelectedGroupPosition, this.mCurrentSelectedChildPosition);
	}

	@Override
	public boolean onGroupClick(ExpandableListView parent, View view,
			int groupPosition, long id) {
		return true;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View view,
			int groupPosition, int childPosition, long id) {
		this.selectItem(groupPosition, childPosition);
		return true;
	}

	public boolean isDrawerOpen() {
		return this.mDrawerLayout != null && this.mDrawerLayout.isDrawerOpen(this.mFragmentContainerView);
	}
	
	public void closeDrawer() {
		this.mDrawerLayout.closeDrawers();
	}

	/**
	 * Users of this fragment must call this method to set up the navigation drawer interactions.
	 *
	 * @param fragmentId   The android:id of this fragment in its activity's layout.
	 * @param drawerLayout The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		this.mFragmentContainerView = this.getActivity().findViewById(fragmentId);
		this.mDrawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer opens
		this.mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener

		final ActionBar actionBar = this.getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);



		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		this.mDrawerToggle = new ActionBarDrawerToggle(
				this.getActivity(),                    /* host Activity */
				this.mDrawerLayout,                    /* DrawerLayout object */
				(Toolbar) this.getActivity().findViewById(R.id.my_awesome_toolbar),             /* nav drawer image to replace 'Up' caret */
				R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
				R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
				) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!NavigationDrawerFragment.this.isAdded()) {
					return;
				}
				NavigationDrawerFragment.this.getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!NavigationDrawerFragment.this.isAdded()) {
					return;
				}

//				if (!NavigationDrawerFragment.this.mUserLearnedDrawer) {
//					// The user manually opened the drawer; store this flag to prevent auto-showing
//					// the navigation drawer automatically in the future.
//					NavigationDrawerFragment.this.mUserLearnedDrawer = true;
//					final SharedPreferences sp = PreferenceManager
//							.getDefaultSharedPreferences(NavigationDrawerFragment.this.getActivity());
//					sp.edit().putBoolean(NavigationDrawerFragment.PREF_USER_LEARNED_DRAWER, true).apply();
//				}

				NavigationDrawerFragment.this.getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
			}
		};

		// If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
		// per the navigation drawer design guidelines.
//		if (!this.mUserLearnedDrawer && !this.mFromSavedInstanceState) {
//			this.mDrawerLayout.openDrawer(this.mFragmentContainerView);
//		}

		// Defer code dependent on restoration of previous instance state.
		this.mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				NavigationDrawerFragment.this.mDrawerToggle.syncState();
			}
		});

		this.mDrawerLayout.setDrawerListener(this.mDrawerToggle);
	}

	private void selectItem(int groupPosition, int childPosition) {
		if (this.mDrawerLayout != null) {
			this.mDrawerLayout.closeDrawer(this.mFragmentContainerView);
		}
		
		// if group position is last, logout
		if (groupPosition == listMenuItemGroups.size() - 1) {
			LogoutUtility.processLogout(this.getActivity());
			return;
		}
						
		if (this.mCallbacks != null) {
			this.mCallbacks.onNavigationDrawerItemSelected(this.mMapMenuItems.get(this.listMenuItemGroups.get(groupPosition)).get(childPosition));
		}
		
		// does not change selection if my account is tapped
		if (groupPosition == 0) {
			return;
		}
		this.mCurrentSelectedGroupPosition = groupPosition;
		this.mCurrentSelectedChildPosition = childPosition;
		this.mMenuItemAdapter.notifyDataSetChanged();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (final ClassCastException e) {
			throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		this.mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(NavigationDrawerFragment.STATE_GROUP_SELECTED_POSITION, this.mCurrentSelectedGroupPosition);
		outState.putInt(NavigationDrawerFragment.STATE_CHILD_SELECTED_POSITION, this.mCurrentSelectedChildPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		this.mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar. See also
		// showGlobalContextActionBar, which controls the top-left area of the action bar.
		if (this.mDrawerLayout != null && this.isDrawerOpen()) {
			inflater.inflate(R.menu.global, menu);
			this.showGlobalContextActionBar();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		if (item.getItemId() == R.id.action_search) {
			Intent intent = new Intent(getActivity(), SearchActivity.class);
			startActivity(intent);
			getActivity().overridePendingTransition(0, 0);
			result = true;
		}
		if (!result) {
			result = this.mDrawerToggle.onOptionsItemSelected(item);
		}

		return result;
	}

	public void selectMenuItem(MenuItemGroup menuItemGroup, MenuItemChild menuItemChild) {
		Set<Map.Entry<MenuItemGroup, ArrayList<MenuItemChild>>> entrySet = mMapMenuItems.entrySet();
		int childIndex = 0;
		int groupIndex = 0;
		for (groupIndex = 0; groupIndex < listMenuItemGroups.size(); groupIndex++) {
			if (listMenuItemGroups.get(groupIndex) == menuItemGroup) {
				break;
			}
		}
		ArrayList<MenuItemChild> childItems = mMapMenuItems.get(listMenuItemGroups.get(groupIndex));
		for (childIndex = 0; childIndex < childItems.size(); childIndex++) {
			if (childItems.get(childIndex) == menuItemChild) {
				break;
			}
		}

		mCurrentSelectedGroupPosition = groupIndex;
		mCurrentSelectedChildPosition = childIndex;
		this.selectItem(mCurrentSelectedGroupPosition, mCurrentSelectedChildPosition);
	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to show the global app
	 * 'context', rather than just what's in the current screen.
	 */
	private void showGlobalContextActionBar() {
		final ActionBar actionBar = ((ActionBarActivity) this.getActivity()).getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(R.string.app_name);
	}

	private ActionBar getActionBar() {
		return ((ActionBarActivity) this.getActivity()).getSupportActionBar();
	}

	private class MenuItemAdapter extends BaseExpandableListAdapter {

		@Override
		public int getGroupCount() {
			return NavigationDrawerFragment.this.mMapMenuItems.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			final ArrayList<MenuItemChild> menuItems = NavigationDrawerFragment.this.mMapMenuItems.get(NavigationDrawerFragment.this.listMenuItemGroups.get(groupPosition));
			int count = 0;
			if (menuItems != null) {
				count = menuItems.size();
			}
			return count;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return NavigationDrawerFragment.this.listMenuItemGroups.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return NavigationDrawerFragment.this.mMapMenuItems.get(NavigationDrawerFragment.this.listMenuItemGroups.get(groupPosition));
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			View view = null;
			final MenuItemGroup menuGroup = NavigationDrawerFragment.this.listMenuItemGroups.get(groupPosition);
			final LayoutInflater layoutInflater = (LayoutInflater) NavigationDrawerFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(MenuItem.getGroupLayout(menuGroup), NavigationDrawerFragment.this.mDrawerListView, false);

			final TextView textView = (TextView) view.findViewById(R.id.text_view_menu_item_title);
			if (textView != null) {
				final String groupTitle = MenuItem.getGroupTitle(menuGroup, NavigationDrawerFragment.this.getActivity());
				textView.setText(groupTitle);
			}

			return view;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			final MenuItemGroup groupItem = NavigationDrawerFragment.this.listMenuItemGroups.get(groupPosition);
			final MenuItemChild menuItemChild = NavigationDrawerFragment.this.mMapMenuItems.get(groupItem).get(childPosition);
			final LayoutInflater layoutInflater = (LayoutInflater) NavigationDrawerFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater.inflate(MenuItem.getChildLayout(menuItemChild), parent, false);
			
			final ImageView imageViewMenuIcon = (ImageView) view.findViewById(R.id.image_view_menu_item_icon);
			final TextView textView = (TextView) view.findViewById(R.id.text_view_menu_item_title);
			
			textView.setText(MenuItem.getChildTitle(menuItemChild, getActivity()));
			
			if (groupItem == MenuItemGroup.ACCOUNT) {
				// TODO update title and icon with current user
				loadUserIcon(imageViewMenuIcon);
				loadUserName(textView);
			} else {
				if (groupPosition == NavigationDrawerFragment.this.mCurrentSelectedGroupPosition && childPosition == NavigationDrawerFragment.this.mCurrentSelectedChildPosition) {
					view.setBackgroundColor(NavigationDrawerFragment.this.getResources().getColor(R.color.menu_item_state_selected));
					imageViewMenuIcon.setImageResource(MenuItem.getChildIconSel(menuItemChild));
				} else {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
						view.setBackground(getResources().getDrawable(R.drawable.selector_menu_items_list));
					} else {
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_menu_items_list));
					}
					imageViewMenuIcon.setImageResource(MenuItem.getChildIcon(menuItemChild));
				}
			}
			return view;
		}
			
		private void loadUserIcon(final ImageView imageView) {
			ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
			imageLoader.get(UrlUtil.getImageUrl(CurrentUser.getCurrentUser().getImageId()), new ImageLoader.ImageListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						if (getActivity() != null) {
							Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_anonymous_avatar_40dp);
							imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
									SizeUtil.getSizeInPixels(50), bitmap));
						}
					}
					
					@Override
					public void onResponse(ImageContainer response, boolean isImmediate) {
						if (getActivity() != null) {
							Bitmap bitmap = response.getBitmap();
							if (bitmap == null) {
								bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_anonymous_avatar_40dp);
							}
							imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
									SizeUtil.getSizeInPixels(50), bitmap));
						}
					}
				}
			);
		}
		
		private void loadUserName(TextView textView) {
			String name = "My Account";
			if (!StringUtil.isNullOrEmpty(CurrentUser.getCurrentUser().getName())) {
				name = CurrentUser.getCurrentUser().getName();
			}
			textView.setText(name);
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

	}

	/**
	 * Callbacks interface that all activities using this fragment must implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(MenuItemChild menuItemChild);
	}

}
