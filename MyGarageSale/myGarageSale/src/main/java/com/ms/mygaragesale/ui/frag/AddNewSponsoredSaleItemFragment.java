package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.util.EndlessAdapter;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.ui.IActivityEventCallback;
import com.ms.mygaragesale.ui.frag.LoadingMessageDialogFragment.DialogFragmentCompleteListener;
import com.ms.mygaragesale.ui.view.FloatingActionButton;

public class AddNewSponsoredSaleItemFragment extends AddNewSaleItemFragment 
		implements OnClickListener, IActivityEventCallback, DialogFragmentCompleteListener {
	
//	private FloatingActionButton buttonAddTags;
	
//	private ArrayList<Integer> listSelectedTagsIndex;
//	private ArrayList<Category> tagsList;
//	private TagsAdapter tagsAdapter;
//	private MGSError error;
//	private AlertDialog dialog;
	
//	private ApiTask<CategoriesResponse> tagsAsyncTask;
//	private String paginationToken;
//	private Integer totalResults;
//	private int limit = 50;
//	private int offset;

	public static AddNewSponsoredSaleItemFragment newInstance() {
		final AddNewSponsoredSaleItemFragment fragment = new AddNewSponsoredSaleItemFragment();
		return fragment;
	}
	
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		listSelectedTagsIndex = new ArrayList<>();
//		tagsList = new ArrayList<>();
//
//		getTagsFromAPI();
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_create_sponsored_sale_item, container, false);
//		this.buttonAddTags = (FloatingActionButton) rootView.findViewById(R.id.action_button_add_tags);
		this.buttonAddImage = (FloatingActionButton) rootView.findViewById(R.id.action_button_add_image);
		this.editTextPostTitle = (EditText) rootView.findViewById(R.id.edit_text_post_title);
		this.editTextPostDescription = (EditText) rootView.findViewById(R.id.edit_text_post_description);
		this.editTextPostPrice = (EditText) rootView.findViewById(R.id.edit_text_post_price);	
		this.editTextPostCategory = (EditText) rootView.findViewById(R.id.edit_text_selected_tags);
		this.gridViewImagePicked = (GridView) rootView.findViewById(R.id.grid_view_image_picked);
		this.initialize();
		return rootView;
	}
	
//	@Override
//	public void onDestroy() {
//		super.onDestroy();
//		if (tagsAsyncTask != null) {
//			tagsAsyncTask.cancel();
//		}
//		if (this.dialog != null) {
//			this.dialog.dismiss();
//			this.dialog = null;
//		}
//	}
	
//	@Override
//	public void onClick(View view) {
//        super.onClick(view);
//		if (view == this.buttonAddTags) {
//			this.selectTagsDialog();
//		}
//	}
	
	@Override
	protected void initialize() {
		super.initialize();
		this.editTextPostCategory.setEnabled(false);
//		this.buttonAddTags.setOnClickListener(this);
	}	
	
	@Override
	protected int getSaleItemSponsored() {
		return SaleItem.SALE_ITEM_SPONSORED;
	}
	
	@Override
	protected void setTitle() {
		final ActionBar actionBar = ((ActionBarActivity) this.getActivity()).getSupportActionBar();
		actionBar.setTitle("Create Sponsored Ad Post");
	}

    @Override
    protected void updateCanSave() {
        boolean oldCanSaveValue = canSaleNewItem;
        if (editTextPostTitle.getText().toString().trim().length() < 4) {
            editTextPostTitle.setError("length must me greater than 3");
        } else {
            editTextPostTitle.setError(null);
        }
        if (editTextPostTitle.getText().toString().length() >= 4
                && editTextPostPrice.getText().toString().length() > 0
                && editTextPostDescription.getText().toString().length() > 0
                && editTextPostCategory.getText().toString().length() > 0) {
            canSaleNewItem = true;
        } else {
            canSaleNewItem = false;
        }
        if (oldCanSaveValue != canSaleNewItem) {
            getActivity().invalidateOptionsMenu();
        }
    }

    private void update() {
//        if (tagsAdapter != null) {
//            if (this.dialog != null) {
//                if (totalResults != null) {
//                    this.dialog.setTitle("Select tags (" + totalResults + ")");
//                } else {
//                    this.dialog.setTitle("Select tags");
//                }
//            }
//
//            tagsAdapter.notifyDataSetChanged();
//            if (tagsList.isEmpty()) {
//                if (this.error != null) {
//                    if (this.dialog != null) {
//                        dialog.dismiss();
//                    }
//                    MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
//                } else {
//                    MGSApplication.showToast("No tag items present to add sponsored sale item. Create tags first.", Toast.LENGTH_LONG);
//                }
//            } else {
//                if (this.error != null) {
//                    MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
//                }
//            }
//        }
	}
	
//	private void getTagsFromAPI() {
//		String url = UrlUtil.getTagsUrl(limit, offset, paginationToken);
//		tagsAsyncTask = new ApiTaskFactory<CategoriesResponse>().newInstance(getActivity(), url,
//				APIMethod.GET, CategoriesResponse.class, new GetTagsResponseHandler());
//		tagsAsyncTask.executeRequest(null, false);
//	}
	
//	private class GetTagsResponseHandler extends ApiResponseHandler<CategoriesResponse> {
//		@Override
//		public void onApiFailure(MGSError error) {
//			super.onApiFailure(error);
//			tagsAsyncTask = null;
//			AddNewSponsoredSaleItemFragment.this.error = error;
//			update();
//		}
//
//		@Override
//		public void onApiSuccess(CategoriesResponse response, boolean isCachedResponse) {
//			super.onApiSuccess(response, isCachedResponse);
//			tagsAsyncTask = null;
//
//            if (offset == 0) {
//                tagsList.clear();
//            }
//
//			if (response != null && response.getCategories() != null) {
//				totalResults = response.getTotalResults();
//				limit = response.getLimit();
//				offset = response.getOffset() + limit;
//				paginationToken = response.getPagination();
//				tagsList.addAll(response.getCategories());
//			}
//			update();
//		}
//	}
//
//	private void selectTagsDialog() {
//		if (tagsList.isEmpty()) {
//			getTagsFromAPI();
//		}
//
//        this.tagsAdapter = new TagsAdapter();
//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//		builder.setTitle("Select tags ("+ totalResults +")");
//		builder.setAdapter(tagsAdapter, null);
//		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				editTextPostCategory.setText(getCategoriesText());
//			}
//		});
//	    builder.setNegativeButton("CANCEL", null);
//	    dialog = builder.create();
//		dialog.getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//		dialog.show();
//	}
//
//    private void toggleTagSelection(View view, int position) {
//        Integer index = Integer.valueOf(position);
//        CheckBox checkBox = (CheckBox) view.findViewById(R.id.check_box_tag);
//        if (listSelectedTagsIndex.contains(index)) {
//            listSelectedTagsIndex.remove(index);
//            if (checkBox != null) {
//                checkBox.setChecked(false);
//            }
//        } else {
//            listSelectedTagsIndex.add(index);
//            if (checkBox != null) {
//                checkBox.setChecked(true);
//            }
//        }
//    }

//    private String getCategoriesText() {
//        StringBuffer text = new StringBuffer();
//        for (int index : listSelectedTagsIndex) {
//            Category category = tagsList.get(index);
//            text.append(category.getCategoryName() + ", ");
//        }
//        // remove last ,
//        try {
//            text.delete(text.length() - 2, text.length() - 1);
//        } catch(Exception e) {
//            if (Logger.DEBUG) {
//                e.printStackTrace();
//            }
//        }
//        return text.toString().trim();
//    }
	
//	private class TagsAdapter extends EndlessAdapter {
//
//		@Override
//		public int getItemCount() {
//			return tagsList.size();
//		}
//
//		@Override
//		public View getItemView(final int position, View convertView, ViewGroup parent) {
//            if (convertView == null || convertView.getTag() == null || !convertView.getTag().equals("TAG_ITEM")) {
//                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.view_tags_selection, null);
//                convertView.setTag("TAG_ITEM");
//            }
//            TextView textViewTagTitle = (TextView) convertView.findViewById(R.id.text_view_tag_title);
//            textViewTagTitle.setText(tagsList.get(position).getCategoryName());
//            CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.check_box_tag);
//            checkBox.setChecked(listSelectedTagsIndex.contains(Integer.valueOf(position)));
//
//            final View convertViewFinal = convertView;
//            convertView.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    toggleTagSelection(convertViewFinal, position);
//                }
//            });
//
//            checkBox.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    toggleTagSelection(convertViewFinal, position);
//                }
//            });
//
//            return convertView;
//		}
//
//		@Override
//		public boolean isMoreDataAvailable() {
//			if (error == null && totalResults > tagsList.size()) {
//				return true;
//			}
////			if (tagsList.isEmpty()) {
////				return true;
////			}
//			return false;
//		}
//
//		@Override
//		public View getProgressView() {
//			ProgressBar progressBar = new ProgressBar(getActivity());
//			progressBar.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, SizeUtil.getSizeInPixels(50)));
//			int paddingDp12 = SizeUtil.getSizeInPixels(12);
//			progressBar.setPadding(paddingDp12, paddingDp12, paddingDp12, paddingDp12);
//			return progressBar;
//		}
//
//		@Override
//		public void loadMoreData() {
//			getTagsFromAPI();
//		}
//	}
}
