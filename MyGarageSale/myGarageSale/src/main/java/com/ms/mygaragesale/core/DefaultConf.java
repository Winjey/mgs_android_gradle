package com.ms.mygaragesale.core;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import android.content.Context;

public class DefaultConf {

	public static final int IMAGE_MAX_WIDTH = 1000;
	public static final int IMAGE_MAX_HEIGHT = 1000;
	public static final String IMAGE_CACHE_DIR = "picked_images";
	public static boolean isTwitterSessionValid;
	public static boolean isFacebookSessionValid;
	
	public static String getImageCachePath() {
		Context context = MGSApplication.getInstance().getApplicationContext();
		UUID uuid = UUID.randomUUID();
		File dir = new File(context.getExternalCacheDir(), DefaultConf.IMAGE_CACHE_DIR);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File file = new File(dir, uuid.toString());
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				if (Logger.DEBUG) {
					e.printStackTrace();
				}
			}
		}
		return file.getAbsolutePath();
	}

	public static void deleteAllImageCaches() {
		try {
			Context context = MGSApplication.getInstance().getApplicationContext();
			File dir = new File(context.getExternalCacheDir(), DefaultConf.IMAGE_CACHE_DIR);
			if (dir.exists()) {
				for (File file : dir.listFiles()) {
					file.delete();
				}
			}
		} catch(Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}

}
