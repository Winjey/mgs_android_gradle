package com.ms.mygaragesale.ui.frag;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.util.DateUtil;
import com.ms.mygaragesale.ui.view.TimestampView;

public abstract class RefreshableItemsWithCountHeaderFragment<T> extends RefreshableItemsFragment<T> {
	
	protected TimestampView timestampViewLastUpdated;
	protected TextView textViewItemsCount;
	
	protected View getHeaderView() {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = (ViewGroup) layoutInflater.inflate(R.layout.grid_view_items_header, null);
		timestampViewLastUpdated = (TimestampView) view.findViewById(R.id.time_stamp_last_updated);
		textViewItemsCount = (TextView) view.findViewById(R.id.text_view_item_count);
		return view;
	}
	
	protected void setLastUpdatedTimeStamp(long lastUpdatedTime) {
		if (timestampViewLastUpdated != null) {
			String updatedTimeString = DateUtil
					.getLocalTimeAndDate(lastUpdatedTime);
			if (updatedTimeString != null) {
				timestampViewLastUpdated.setVisibility(View.VISIBLE);
				timestampViewLastUpdated.setTimestamp(updatedTimeString);

			} else {
				timestampViewLastUpdated.setVisibility(View.GONE);
			}
		}
	}

	protected void setItemsCountTitle() {
		if (textViewItemsCount != null) {
			textViewItemsCount.setText(getResources().getQuantityString(
					R.plurals.item_count_plural, listItems.size(),
					listItems.size()));
		}
	}
	
	@Override
	protected void update() {
		if (this.getActivity() != null) {
			super.update();
			this.setItemsCountTitle();
			this.setLastUpdatedTimeStamp(getLastUpdatedTime());
		}
	}
}
