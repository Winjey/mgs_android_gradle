package com.ms.mygaragesale.model;

import java.util.Date;

import com.ms.mygaragesale.model.response.Response;

public class Message extends Response {
	
	public static final int MESSAGE_TYPE_PRIVATE = 1;
	public static final int MESSAGE_IN_SALE_ITEM = 2;
	
	private String message;
	private String messageId;
	private Date createdDate;
	private int messageType;
	private User sender;
	private String receiverId;
	
	public Message() {
		// TODO Auto-generated constructor stub
	}

	public Message(String message, String messageId, Date createdDate,
			int messageType, User sender, String receiverId) {
		super();
		this.message = message;
		this.messageId = messageId;
		this.createdDate = createdDate;
		this.messageType = messageType;
		this.sender = sender;
		this.receiverId = receiverId;
	}



	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}



	public User getSender() {
		return sender;
	}



	public void setSender(User sender) {
		this.sender = sender;
	}



	public String getReceiverId() {
		return receiverId;
	}



	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
	
}
