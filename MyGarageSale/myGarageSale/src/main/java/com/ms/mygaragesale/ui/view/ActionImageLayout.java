package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ms.garagesaledeal.R;

public class ActionImageLayout extends RelativeLayout implements View.OnClickListener {

	private ImageView imageView;
	private ImageButton imageButton;
	private OnImageActionButtonListener onImageActionButtonListener;
	
	private Uri currentUri;

	public ActionImageLayout(Context context) {
		super(context);
		this.initialize(null);
	}

	public ActionImageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initialize(attrs);
	}

	public ActionImageLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.initialize(attrs);
	}
	
	public ImageView getImageView() {
		return imageView;
	}
	
	public void setOnImageActionButtonListener(OnImageActionButtonListener onImageActionButtonListener) {
		this.onImageActionButtonListener = onImageActionButtonListener;
	}

	public void setImageUri(Uri uri) {
		this.currentUri = uri;
		this.imageView.setImageURI(this.currentUri);
	}
	
	public Uri getImageUri() {
		return this.currentUri;
	}
	
	public void setActionImageResource(int resId) {
		imageButton.setImageResource(resId);
		//this.imageButton.setScaleType(ScaleType.CENTER);
	}
	
	public void setActionButtonEnabled(boolean enable) {
		if (enable) {
			imageButton.setVisibility(View.VISIBLE);
		} else {
			imageButton.setVisibility(View.INVISIBLE);
		}
	}

	private void initialize(AttributeSet attrs) {
		this.imageView = new ImageView(this.getContext(), attrs);
		this.imageView.setId(Integer.MIN_VALUE);
		this.imageView.setMaxWidth(this.getSizeInDP(110));
		this.imageView.setMaxHeight(this.getSizeInDP(110));
		this.addView(this.imageView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		
		this.imageButton = new ImageButton(this.getContext());
		this.imageButton.setAlpha(0.8f);
		this.imageButton.setOnClickListener(this);
		this.imageButton.setBackgroundResource(R.drawable.selector_card_grey);
		this.imageButton.setImageResource(R.drawable.ic_close_24dp);
		LayoutParams layoutParams = new LayoutParams(this.getSizeInDP(25), this.getSizeInDP(25));
		layoutParams.setMargins(0, 0, 1, 1);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		this.imageButton.setLayoutParams(layoutParams);
		this.addView(this.imageButton);
	}

	private int getSizeInDP(int pixels) {
		float density = this.getResources().getDisplayMetrics().density;
		return (int)(pixels * density);
	}

	@Override
	public void onClick(View v) {
		if (this.onImageActionButtonListener != null) {
			this.onImageActionButtonListener.onImageActionClick(this);
		}
	}
	
	
	public interface OnImageActionButtonListener {
		
		public abstract void onImageActionClick(ActionImageLayout actionImageLayout);
		
	}

}
