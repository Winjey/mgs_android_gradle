package com.ms.mygaragesale.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.util.LogoutUtility;

public class AccountBlockedActivity extends MGSActivity implements
		OnClickListener {

	private Button buttonLogout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_account_blocked);
		this.setTitle("Account Blocked");
		buttonLogout = (Button) findViewById(R.id.button_logout);
		buttonLogout.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View view) {
		if (view == buttonLogout) {
			LogoutUtility.processLogout(this);
		}
	}

}
