package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;

/**
 * Abstract base class to display common views like loading view, message view
 * and content view on landing page.
 */
public abstract class RefreshableItemsFragment<T> extends RefreshableFragment implements
		OnItemClickListener {

	protected AbsListView absListView;
	protected View headerView;
	protected View footerView;
	
	protected ArrayList<T> listItems;
	protected ItemsAdapter itemsAdapter;
	
	protected MGSError error;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listItems = new ArrayList<>();
		itemsAdapter = new ItemsAdapter();
	}
	
	@Override
	protected final View getContentView() {
		this.absListView = getAbsListView();
		return absListView;
	}
	
	protected View getHeaderView() {
		return null;
	}
	
	protected View getFooterView() {
		return null;
	}
	@Override
	protected void initialize() {
		this.headerView = this.getHeaderView();
		if (this.headerView != null) {
			if (absListView instanceof ListView) {
				((ListView) absListView).addHeaderView(this.headerView, null, false);
			} else if (absListView instanceof StaggeredGridView) {
				((StaggeredGridView) absListView).addHeaderView(this.headerView, null, false);
			}
		}
		
		this.footerView = this.getFooterView();
		if (this.footerView != null) {
			if (absListView instanceof ListView) {
				((ListView) absListView).addFooterView(this.footerView, null, false);
			} else if (absListView instanceof StaggeredGridView) {
				((StaggeredGridView) absListView).addFooterView(this.footerView, null, false);
			}
		}
		
		this.absListView.setOnItemClickListener(this);
		this.absListView.setAdapter(itemsAdapter);
	}
			
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
	}

	/**
	 * Override in child class to add view to display other than loading and
	 * message view. This method is called during initialization of view, i.e.
	 * in method initialized.
	 * 
	 * @return View - to be added to content view
	 */
	protected abstract AbsListView getAbsListView();

	//protected abstract ArrayList<T> getListItems();

	protected abstract View getItemView(int position, T item, View convertView,
			ViewGroup parent);
	
	protected int getItemCount() {
        if (listItems != null) {
            return listItems.size();
        }
		return 0;
	}
	
	protected void update() {
		if (getActivity() != null) {
			itemsAdapter.notifyDataSetChanged();

			if (listItems == null || listItems.isEmpty()) {
				if (error != null) {
					showErrorMessage();
				} else {
					this.displayNoItemsMessage();
				}
				showView(ContentViewType.CONTENT_MESSAGE);
			} else {
				if (error != null) {
					toastErrorMessage();
				}
				showView(ContentViewType.CONTENT_DATA);
			}
		}
	}

	protected void displayNoItemsMessage() {
		messageView.showMessage("No Items Found", R.drawable.ic_error_sync, false);
	}
	
	protected long getLastUpdatedTime() {
		return 0;
	}
	
	/**
	 * Method to switch between content views like loading, message and list
	 * view.
	 * 
	 * @param viewType
	 *            - {@link ContentViewType}
	 */
	protected final void showView(ContentViewType viewType) {
		switch (viewType) {
		case CONTENT_LOADING:
			viewSwitcher.setDisplayedChild(ContentViewType.CONTENT_LOADING
					.getValue());
			break;
		case CONTENT_DATA:
			viewSwitcher.setDisplayedChild(ContentViewType.CONTENT_DATA
					.getValue());
			messageView.setVisibility(View.INVISIBLE);
			if (headerView != null) {
				headerView.setVisibility(View.VISIBLE);
			}
			break;
		case CONTENT_MESSAGE:
			viewSwitcher.setDisplayedChild(ContentViewType.CONTENT_MESSAGE
					.getValue());
			messageView.setVisibility(View.VISIBLE);
			if (headerView != null) {
				if (error != null) {
					headerView.setVisibility(View.GONE);
				} else {
					headerView.setVisibility(View.VISIBLE);
				}
			}
			break;
		}
	}

	protected void showErrorMessage() {
		messageView.showMessage(error);
	}

	protected void toastErrorMessage() {
		String errorMessage = error.getMesssage();
		MGSApplication.showToast(errorMessage, Toast.LENGTH_LONG);
	}

	/**
	 * Enum to indicate different content views.
	 */
//	public enum ContentViewType {
//		// index for CONTENT_DATA, CONTENT_MESSAGE are same since these both
//		// views are displayed at same time using viewSwitcher.
//		CONTENT_LOADING(0), CONTENT_DATA(1), CONTENT_MESSAGE(1);
//
//		private int value;
//
//		private ContentViewType(int value) {
//			this.value = value;
//		}
//
//		public int getValue() {
//			return this.value;
//		}
//	}

	public class ItemsAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return getItemCount();
		}

		@Override
		public Object getItem(int position) {
			return listItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			T item = null;
			if (position < listItems.size()) {
				item = listItems.get(position);
			}
			return getItemView(position, item, convertView, parent);
		}
	}
}
