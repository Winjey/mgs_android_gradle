package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ms.garagesaledeal.R;


public class LoadingView extends RelativeLayout {
	
	private ProgressBar progressBar;
	private TextView textViewLoadingMessage;
	
	public LoadingView(Context context) {
		super(context);
		init(null);
	}

	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
		
	}
	
	public LoadingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}
	
//	public void showLoading() {
//		this.progressBar.setVisibility(View.VISIBLE);
//	}
//	
//	public void hideLoading() {
//		this.progressBar.setVisibility(View.GONE);
//	}
	
	public void setLoadingMesage(String message) {
		this.textViewLoadingMessage.setText(message);
	}
	
	public void hideLoadingMesage() {
		this.textViewLoadingMessage.setVisibility(View.GONE);
	}
	
	public void showLoadingMesage() {
		this.textViewLoadingMessage.setVisibility(View.VISIBLE);
	}
	
	private void init(AttributeSet attrs) {
		inflate(getContext(), R.layout.view_loading, this);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar_loading);
		textViewLoadingMessage = (TextView) findViewById(R.id.text_view_loading_message);
		if (attrs != null) {
			TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.LoadingView);
			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) progressBar.getLayoutParams();
			layoutParams.width = typedArray.getDimensionPixelSize(R.styleable.LoadingView_progressWidth, layoutParams.width);
			layoutParams.height = typedArray.getDimensionPixelSize(R.styleable.LoadingView_progressHeight, layoutParams.height);
			progressBar.requestLayout();
			typedArray.recycle();
		}
		
		this.hideLoadingMesage();
	}
}
