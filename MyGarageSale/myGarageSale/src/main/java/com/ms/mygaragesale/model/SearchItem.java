package com.ms.mygaragesale.model;

public class SearchItem implements Comparable<SearchItem> {
	
	private String keyword;
	private String category;
	
	public SearchItem() {
	}
	
	public SearchItem(String keyword) {
		super();
		this.keyword = keyword;
		this.category = "";
	}
	
	public SearchItem(String keyword, String category) {
		super();
		this.keyword = keyword;
		this.category = category;
	}

	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public int compareTo(SearchItem another) {
		return this.keyword.compareTo(another.getKeyword());
	}
	
	

}
