package com.ms.mygaragesale.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.ms.garagesaledeal.R;

/**
 * This activity is translucent full screen activity and it slides from right side of device.
 * This activity is displayed similar to open navigation drawer.
 */
public class DetailActivity extends MGSActivity {
	
	public static final String KEY_DETAIL_ACTIVITY_BUNDLE = "KEY_DETAIL_ACTIVITY_BUNDLE";
	
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		this.setContentView(R.layout.activity_detail);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.initialize();
	}
	
	private void initialize() {
		Bundle bundle = getIntent().getBundleExtra(KEY_DETAIL_ACTIVITY_BUNDLE);
		if (bundle == null) {
			throw new IllegalArgumentException("Intent does not contain any bundle data with key " + KEY_DETAIL_ACTIVITY_BUNDLE);
		}
		Object object = bundle.getSerializable(ActivityBundleFactory.KEY_FRAGMENT_CLASS_NAME);
		if (object == null) {
			throw new IllegalArgumentException("Bundle does not contains any fragment class name.");
		}
		
		try {
			FragmentManager fragmentManager = getFragmentManager();
			fragment = fragmentManager.findFragmentById(R.id.layout_container);
			if (fragment == null) {
				Class<Fragment> fragmentClass = (Class<Fragment>) object;
				fragment = fragmentClass.newInstance();
				fragment.setArguments(bundle);
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.layout_container, fragment);
				fragmentTransaction.commit();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new IllegalArgumentException(ex.getMessage());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		if (item.getItemId() == android.R.id.home) {
			this.onBackPressed();
			result = true;
		}
		return result;
	}
	

	
//	/**
//	 * Implement this fragment in order to add title to SlidingOverlayActivity. 
//	 */
//	public static interface ISlidingActivityFragment {
//		
//		public abstract String getTitle();
//		
//	}
		
}
