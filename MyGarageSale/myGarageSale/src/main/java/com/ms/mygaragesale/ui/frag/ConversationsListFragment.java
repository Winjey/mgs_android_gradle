package com.ms.mygaragesale.ui.frag;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.DateUtil;
import com.ms.mygaragesale.core.util.ImageUtil;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.response.Conversation;
import com.ms.mygaragesale.model.response.ConversationsResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;

public class ConversationsListFragment extends RefreshableItemsWithCountHeaderFragment<Conversation> 
			implements OnClickListener {
	
	private ApiTask<ConversationsResponse> conversationsApiTask;
	
	public static ConversationsListFragment newInstance() {
		return new ConversationsListFragment();
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.setRetainInstance(true);   	
    	getConversationsListFromApi(true);
    }
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
    	Conversation conversation = listItems.get(index - 1);
//    	if (conversation.getConvType() == Conversation.CONVERSATION_TYPE_IN_SALE_ITEM) {
			Intent intent = new Intent(getActivity(), DetailActivity.class);
			Bundle bundle = ActivityBundleFactory.getBundle(conversation.getConvId(), conversation, ViewType.MESSAGE_IN_CONVERSATION);
			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
			startActivityForResult(intent, UserAccountFragment.REQUEST_CODE_USER_DETAIL);
//    	} else {
//    		String userId = null;
//    		if (conversation.getEndOne().getId().equals(CurrentUser.getCurrentUser().getUserId())) {
//				userId = conversation.getEndAnother().getId();
//			} else {
//				userId = conversation.getEndOne().getId();
//			}
//    		Intent intent = new Intent(getActivity(), DetailActivity.class);
//			Bundle bundle = ActivityBundleFactory.getBundle(userId, null, ViewType.MESSAGE_BETWEEN_USER);
//			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
//			startActivityForResult(intent, UserAccountFragment.REQUEST_CODE_USER_DETAIL);
//    	}
    }
    
    @Override
    protected void update() {
    	if (conversationsApiTask == null && getActivity() != null) {
    		super.update();
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (conversationsApiTask != null) {
    		conversationsApiTask.cancel();
    		conversationsApiTask = null;
    	}
    }
    
    @Override
	protected long getLastUpdatedTime() {
		long lastUpdatedTime = 0;
		lastUpdatedTime = new AppSharedPref().getConversationsLastUpdatedTime();
		return lastUpdatedTime;
	}
	
	private void setLastUpdatedTime(long time) {
		new AppSharedPref().setConversationsLastUpdatedTime(time);
	}
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if (requestCode == UserAccountFragment.REQUEST_CODE_USER_DETAIL && resultCode == Activity.RESULT_OK) {
			getConversationsListFromApi(false);
		}
    }
    
    @Override
    protected AbsListView getAbsListView() {
    	LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	return (AbsListView) layoutInflater.inflate(R.layout.list_view_messages, null);
    }
    
	@Override
	protected View getItemView(int position, Conversation conversation, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.view_conversations, parent, false);
		}
		TextView text_view_user_name = (TextView) convertView.findViewById(R.id.text_view_conversation_name);
		TextView text_view_pending_request_text = (TextView) convertView.findViewById(R.id.text_view_last_message_text);
		TextView text_view_last_message_created_text = (TextView) convertView.findViewById(R.id.text_view_last_message_created_text);
		ImageView image_view_user_icon = (ImageView) convertView.findViewById(R.id.image_view_conversation_icon);
		
		String name = null;
		String messageText = null;
		String imageId = null;
		int defaultImageResId = R.drawable.avatar_blue_120;
		if (conversation.getConvType() == Conversation.CONVERSATION_TYPE_BETWEEN_USERS) {
			if (conversation.getEndOne().getId().equals(CurrentUser.getCurrentUser().getUserId())) {
				name = conversation.getEndAnother().getName();
				imageId = conversation.getEndAnother().getImageId();
			} else {
				name = conversation.getEndOne().getName();
				imageId = conversation.getEndOne().getImageId();
			}
		} else {
			name = conversation.getEndAnother().getName();
			imageId = null;
			defaultImageResId = R.drawable.ic_conv_sale_item;
		}
		
		if (conversation.getLastMessage().getSenderId().equals(CurrentUser.getCurrentUser().getUserId())) {
			messageText = "me: " + conversation.getLastMessage().getMessage();
		} else {
			messageText = conversation.getLastMessage().getMessage();
		}
		
		text_view_user_name.setText(name);
		text_view_pending_request_text.setText(messageText);
		text_view_last_message_created_text.setText(DateUtil.getDateTimeText(conversation.getLastMessage().getCreatedDate()));
		loadImage(imageId, image_view_user_icon, defaultImageResId);
				
		return convertView;
	}
 	
	private void loadImage(String imageId, final ImageView imageView, final int defaultIconResId) {
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		if (imageId == null) {
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), defaultIconResId);
			imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
					SizeUtil.getSizeInPixels(50), bitmap));
		} else {
			imageLoader.get(UrlUtil.getImageUrl(imageId), 
				new ImageLoader.ImageListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						if (getActivity() != null) {
							Bitmap bitmap = BitmapFactory.decodeResource(getResources(), defaultIconResId);
							imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
									SizeUtil.getSizeInPixels(50), bitmap));
						}
					}
					
					@Override
					public void onResponse(ImageContainer response, boolean isImmediate) {
						if (getActivity() != null) {
							Bitmap bitmap = response.getBitmap();
							if (bitmap == null) {
								bitmap = BitmapFactory.decodeResource(getResources(), defaultIconResId);
							}
							imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
									SizeUtil.getSizeInPixels(50), bitmap));
						}
					}
				}
			);
		}
	}
		
	private void getConversationsListFromApi(boolean shouldResultCacheResponse) {
		String url = null;
		url = UrlUtil.getConversations();
		conversationsApiTask = new ApiTaskFactory<ConversationsResponse>().newInstance(getActivity(), 
				url, APIMethod.GET, ConversationsResponse.class, new GetConversationsResponseHandler());
		conversationsApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	private class GetConversationsResponseHandler extends ApiResponseHandler<ConversationsResponse> {
		
		@Override
		public void onApiSuccess(ConversationsResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			conversationsApiTask = null;
			ConversationsListFragment.this.error = null;
			
			if (!isCachedResponse) {
				setLastUpdatedTime(System.currentTimeMillis());
				swipeRefreshLayout.setRefreshing(false);
			} else {
				swipeRefreshLayout.setRefreshing(true);
			}
						
			listItems.clear();
			if (response.getConversations() != null) {
				listItems.addAll(response.getConversations());
			}
			update();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			conversationsApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			ConversationsListFragment.this.error = error;
			update();
		}
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		showView(ContentViewType.CONTENT_LOADING);
		getConversationsListFromApi(true);
	}
	
	@Override
	protected void setItemsCountTitle() {
		if (textViewItemsCount != null) {
			textViewItemsCount.setText(getResources().getQuantityString(
					R.plurals.conversations_count_plural, listItems.size(),
					listItems.size()));
		}
	}
	
	@Override
	public void onRefresh() {
		super.onRefresh();
		getConversationsListFromApi(false);
	}

	@Override
	protected void displayNoItemsMessage() {
		messageView.showMessage("No Messages Found", R.drawable.ic_message_grey600_48dp, false);
	}

	@Override
	protected void initialize() {
		super.initialize();
		this.swipeRefreshLayout.setEnabled(true);
		update();
	}
}
