package com.ms.mygaragesale.core;

import io.fabric.sdk.android.Fabric;
import android.app.Application;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

public class MGSApplication extends Application {

	private static MGSApplication applicationInstance;
	private static final String consumerKey = "SRwVmfoZkFE41T6PEnhjIMmYV";
	private static final String consumerSecret = "6yDV03LqB7r0HYFwra1j1wBEcNzK1frGgkFtdtiazc7gNMoads";
	
	public MGSApplication() {
		applicationInstance = this;
	}
	
	public static MGSApplication getInstance() {
		return MGSApplication.applicationInstance;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
//		this.setupTwitter();
//		this.validateTwitterSession();
	}
	
	private void setupTwitter() {
		TwitterAuthConfig authConfig = new TwitterAuthConfig(consumerKey, consumerSecret);
		Fabric.with(this, new Twitter(authConfig));
	}
	
	private void validateTwitterSession() {
		try {
			DefaultConf.isTwitterSessionValid = true;
		} catch(Exception e) {
		}
	}
	
	public static void showToast(String message, int duration) {
		if (applicationInstance != null) {
			Toast.makeText(applicationInstance.getApplicationContext(), message, duration).show();
		}
	}
}
