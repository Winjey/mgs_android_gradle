package com.ms.mygaragesale.model.response;

import java.util.ArrayList;

import com.ms.mygaragesale.model.SaleItem;

public class SaleItemsResponse extends Response {
	
	private int totalResults;
	private int limit;
	private int offset;
	private ArrayList<SaleItem> saleItems;
	
	public SaleItemsResponse() {
		// TODO Auto-generated constructor stub
	}

	public SaleItemsResponse(int totalResults, int limit, int offset,
			ArrayList<SaleItem> saleItems) {
		super();
		this.totalResults = totalResults;
		this.limit = limit;
		this.offset = offset;
		this.saleItems = saleItems;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public ArrayList<SaleItem> getSaleItems() {
		return saleItems;
	}
	
	public void setSaleItems(ArrayList<SaleItem> saleItems) {
		this.saleItems = saleItems;
	}
	
}
