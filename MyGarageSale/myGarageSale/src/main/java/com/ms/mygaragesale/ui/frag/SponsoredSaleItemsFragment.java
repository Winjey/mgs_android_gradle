package com.ms.mygaragesale.ui.frag;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.response.SaleItemsResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;

public class SponsoredSaleItemsFragment extends SaleItemsFragment implements OnClickListener {
	
	private ApiTask<SaleItemsResponse> saleItemApiTask;

	public static SponsoredSaleItemsFragment newInstance() {
		final SponsoredSaleItemsFragment fragment = new SponsoredSaleItemsFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(KEY_SALE_ITEM_TYPE, SaleItemViewType.SPONSORED_SALE_ITEM.getValue());
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected long getLastUpdatedTime() {
		long lastUpdatedTime = 0;
		lastUpdatedTime = new AppSharedPref().getSaleItemsLastUpdatedTime(SaleItemViewType.SPONSORED_SALE_ITEM);	
		return lastUpdatedTime;
	}
	
	private void setLastUpdatedTime(long time) {
		new AppSharedPref().setSaleItemsLastUpdatedTime(SaleItemViewType.SPONSORED_SALE_ITEM, time);
	}
	
	@Override
	protected void getSaleItemFromApi(boolean shouldResultCacheResponse) {
		String url = UrlUtil.getSaleItemsUrl(0, 
					CurrentUser.getCurrentUser().getLatitude(), CurrentUser.getCurrentUser().getLatitude(), 1);
		saleItemApiTask = new ApiTaskFactory<SaleItemsResponse>().newInstance(getActivity(), 
				url, APIMethod.GET, SaleItemsResponse.class, new GetSaleItemResponseHandler());
		saleItemApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	@Override
	protected void setTitle() {
		((ActionBarActivity) getActivity()).setTitle("Sponsored Sale Items");
	}
	
	@Override
	public void onClick(View view) {
		if (view == floatingActionButton) {
			final Intent intent = new Intent(SponsoredSaleItemsFragment.this.getActivity(), DetailActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable(ActivityBundleFactory.KEY_FRAGMENT_CLASS_NAME, AddNewSaleItemFragment.class);
			bundle.putBoolean(AddNewSaleItemFragment.KEY_CREATE_SPONSORED_ITEM, true);
			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
			SponsoredSaleItemsFragment.this.startActivity(intent);
		}
	}

//	@Override
//	protected View getItemView(final int position, final SaleItem saleItem, View convertView, final ViewGroup parent) {
//		View view = convertView;
//		ViewHolder viewHolder = null;
//		if (view == null) {
//			final LayoutInflater layoutInflater = (LayoutInflater) SponsoredSaleItemsFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			view = layoutInflater.inflate(R.layout.view_sale_items, parent, false);
//			viewHolder = new ViewHolder();
//			viewHolder.textViewSaleItemTitle = (TextView) view.findViewById(R.id.text_view_sale_item_title);
//			viewHolder.textViewSaleItemPrice = (TextView) view.findViewById(R.id.text_view_sale_item_price);
//			viewHolder.textViewSaleItemModifiedDate = (TextView) view.findViewById(R.id.text_view_sale_item_modified_date);
//			viewHolder.viewSeparator = view.findViewById(R.id.view_separator);
//			viewHolder.imageViewSaleItemOne = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_one);
//			viewHolder.imageViewSaleItemTwo = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_two);
//			viewHolder.imageViewSaleItemThree = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_three);
//			viewHolder.layout_sale_items_images = (ViewGroup) view.findViewById(R.id.layout_sale_items_images);
//			viewHolder.imageButtonContactSeller = (ImageButton) view.findViewById(R.id.image_button_email_seller);
//			viewHolder.layoutSideImages = (ViewGroup) view.findViewById(R.id.layout_side_images);
//			view.setTag(viewHolder);
//		} else {
//			viewHolder = (ViewHolder) view.getTag();
//		}
//		if (hashSetForOneTimeAnimation.add(position)) {
//			view.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_bottom));
//		}
//		
//		viewHolder.textViewSaleItemTitle.setText(saleItem.getTitle());
//		viewHolder.textViewSaleItemPrice.setText("$ " + saleItem.getPrice());
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm");
//		
//		String categories = getCategoriesText(saleItem);
//		if (StringUtil.isNullOrEmpty(categories)) {
//			viewHolder.textViewSaleItemModifiedDate.setVisibility(View.GONE);
//		} else {
//			viewHolder.textViewSaleItemModifiedDate.setVisibility(View.VISIBLE);
//			viewHolder.textViewSaleItemModifiedDate.setText(categories);
//		}
//		
//		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
//		
//		ArrayList<String> imageIds = saleItem.getImages();
//		if (imageIds == null || imageIds.isEmpty()) {
//			viewHolder.layout_sale_items_images.setVisibility(View.GONE);
//			viewHolder.viewSeparator.setVisibility(View.GONE);
//		} else {
//			viewHolder.layout_sale_items_images.setVisibility(View.VISIBLE);
//			viewHolder.viewSeparator.setVisibility(View.VISIBLE);
//			int imagesCount = imageIds.size();
//			viewHolder.imageViewSaleItemOne.setVisibility(View.VISIBLE);
//			viewHolder.imageViewSaleItemOne.setImageUrl(UrlUtil.getImageUrl(imageIds.get(0)), 
//					imageLoader);
//			
//			if (imagesCount > 1) {
//				viewHolder.layoutSideImages.setVisibility(View.VISIBLE);
//				viewHolder.imageViewSaleItemTwo.setVisibility(View.VISIBLE);
//				viewHolder.imageViewSaleItemTwo.setImageUrl(UrlUtil.getImageUrl(imageIds.get(1)), 
//						imageLoader);
//			} else {
//				viewHolder.layoutSideImages.setVisibility(View.GONE);
//				viewHolder.imageViewSaleItemTwo.setVisibility(View.GONE);
//				viewHolder.imageViewSaleItemThree.setVisibility(View.GONE);
//			}
//			if (imagesCount > 2) {
//				viewHolder.layoutSideImages.setVisibility(View.VISIBLE);
//				viewHolder.imageViewSaleItemThree.setVisibility(View.VISIBLE);
//				viewHolder.imageViewSaleItemThree.setImageUrl(UrlUtil.getImageUrl(imageIds.get(2)), 
//						imageLoader);
//			} else {
//				viewHolder.imageViewSaleItemThree.setVisibility(View.GONE);
//			}
//		} 		
//		
//		final View currentView = view;
//		// enabling item click of grid view
//		
////		// TODO find out better solution than this
//		SaleItemViewClickListener listener = new SaleItemViewClickListener((AdapterView<?>) parent, currentView, position);
//			
//		view.setOnClickListener(listener);
//		
//		// hide mail icon if sale item is created by me
//		if (saleItem.getSeller().getUserId().equals(CurrentUser.getCurrentUser().getUserId())) {
//			viewHolder.imageButtonContactSeller.setVisibility(View.GONE);
//		} else {
//			// hiding/showing emmail id 
//			if (StringUtil.isNullOrEmpty(saleItem.getSeller().getEmailId())) {
//				viewHolder.imageButtonContactSeller.setVisibility(View.GONE);
//			} else {
//				viewHolder.imageButtonContactSeller.setVisibility(View.VISIBLE);
//			}
//		}
//		
//		viewHolder.imageButtonContactSeller.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(getActivity(), DetailActivity.class);
//				Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getSeller().getUserId(), saleItem.getSeller(), ViewType.MESSAGE_BETWEEN_USER);
//				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
//				startActivity(intent);
//			}
//		});
//		
//		return view;
//	}
	
	private class SaleItemViewClickListener implements OnClickListener {
		
		private AdapterView<?> parent;
		private View currentView;
		private int position;
				
		public SaleItemViewClickListener(AdapterView<?> parent,
				View currentView, int position) {
			super();
			this.parent = parent;
			this.currentView = currentView;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			SponsoredSaleItemsFragment.this.onSaleItemClick((AdapterView<?>) parent, currentView, position + 1, currentView.getId());
		}
	}
	
	private static class ViewHolder {
		TextView textViewSaleItemTitle;
		TextView textViewSaleItemPrice;
		TextView textViewSaleItemModifiedDate;
		View viewSeparator;
		ImageButton imageButtonContactSeller;
		NetworkImageView imageViewSaleItemOne;
		NetworkImageView imageViewSaleItemTwo;
		NetworkImageView imageViewSaleItemThree;
		ViewGroup layout_sale_items_images;
		HorizontalScrollView scroll_view_sale_items_images;
		ViewGroup layoutSideImages;
	}
	
	private class GetSaleItemResponseHandler extends ApiResponseHandler<SaleItemsResponse> {
		
		@Override
		public void onApiSuccess(SaleItemsResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			saleItemApiTask = null;
			SponsoredSaleItemsFragment.this.error = null;
			if (!isCachedResponse) {
				setLastUpdatedTime(System.currentTimeMillis());
				swipeRefreshLayout.setRefreshing(false);
				swipeRefreshLayout.setEnabled(true);
			} else {
				swipeRefreshLayout.setRefreshing(true);
			}
			
			listItems.clear();
			if (response.getSaleItems() != null) {
				listItems.addAll(response.getSaleItems());
			}
			
			if (getActivity() != null) {
				update();
			}
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			swipeRefreshLayout.setEnabled(true);
			saleItemApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			SponsoredSaleItemsFragment.this.error = error;
			if (getActivity() != null) {
				update();
			}
		}
	}
}
